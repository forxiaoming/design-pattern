## Prototype Pattern (原型模式)

### 0. ADMail (发送广告邮件)
批量发送邮件案例

[./ad/mail](./ad/mail)

### 1. What?
Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.
(用原型实例指定创建对象的种类, 并且通过拷贝这些原型创建新的对象.)
 
 使用原型实现```Cloneable```接口, 然后重写Object的clone方法实现对象的拷贝

### 2. Use ?

#### 2.1. Advantage ?

* 性能优良:
    * 在内存二进制流中拷贝, 比直接new对象性能高.
* 逃避构造函数的约束:
    * 优点与缺点并存, 内存中拷贝, 不会执行构造函数.
    
#### 2.2. Use sence ?

* 资源优化场景:
    * 初始化需要消耗许多资源.
* 性能和安全要求: 
    * 直接new比较繁琐时.
* 一个对象多个修改者:
    * 如开头案例; 并且一般于工厂方法模式一起出现;
    
#### 3. Attention ? 

* 构造不会被执行
* 浅拷贝和深度拷贝
    * 浅拷贝: 只是拷贝本对象, 不拷贝内部数组及引用对象, 即对象共享私有变量.
    * 深拷贝: 完全拷贝.
    * 建议: 分开实现, 不要混合使用.
[./clone/Test.java](./clone/Test.java)

* clone和final两个冤家
    *　要使用clone(), 成员变量就不要增加final关键字.