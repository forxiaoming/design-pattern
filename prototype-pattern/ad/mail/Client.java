package ad.mail;

import java.util.Random;

public class Client {
    // 发送邮件数量
    private static int MAX_COUNT = 6;


    public static void main(String[] args) {
        // 模拟发送邮件
        int i = 0;
        Mail mail = new Mail(new AdvTemplate());
        mail.setTail("XX银行版权所有");
        while (i < MAX_COUNT) {
            Mail cloneMail = mail.clone();

            cloneMail.setAppellation(getRandomStr(8));
            cloneMail.setReceiver(getRandomStr(5) + "@" + getRandomStr(8).toLowerCase() + ".com");
            sendMail(cloneMail);
            i++;
        }

    }

    public static void sendMail(Mail mail) {
        System.out.println("标题: " + mail.getSubject() + "\t收件人: " + mail.getReceiver() + "\t发送成功! ");
    }

    public static String getRandomStr(int maxLength) {
        final  String  resource = "abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ";
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < maxLength; i++) {
            sb.append(resource.charAt(random.nextInt(resource.length())));
        }
        return sb.toString();
    }
}
