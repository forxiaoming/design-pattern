package three.know.how;

/**
 * 妙计(策略)接口
 */
public interface IStrategy {
    // 每个锦囊妙计的可执行的算法
    public void operate();
}
