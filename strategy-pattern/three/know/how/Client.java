package three.know.how;

/**
 * 场景类-赵云使用锦囊妙计
 */
public class Client {
    public static void main(String[] args) {
        // 拿到锦囊
        Context context;

        context = new Context(new BackDoor());
        context.operate();

        context = new Context(new GivenGreenLight());
        context.operate();

        context = new Context(new BlockEnemy());
        context.operate();
    }
}
