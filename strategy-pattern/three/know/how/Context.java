package three.know.how;

/**
 * 锦囊(装妙计)
 */
public class Context {
    // 要使用的妙计
    private IStrategy strategy;

    public Context(IStrategy strategy) {
        this.strategy = strategy;
    }

    // 使用妙计
    public void operate() {
        this.strategy.operate();
    }
}
