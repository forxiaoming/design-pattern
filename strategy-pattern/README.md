# Strategy Pattern (适配器模式)

## 0. 刘备江东娶妻, 诸葛亮的三个锦囊

![./three/know/how/18-2.png](./three/know/how/18-2.png)

## 1. What?

* Define a family of algorithms , encapsulate each one, and make them interchangeable.
    (定义一组算法, 将每个算法都封装起来, 并且使它们之间可以互相互换.)

* 也叫政策模式(Policy Pattern).
* 采用了面向对象的继承和多态机制.

* **角色**
    * Context 封装角色(上下文角色): 
    * Strategy 抽象策略角色
    * ConcreteStrategy 具体策略角色

![./three/know/how/18-3.png](./three/know/how/18-3.png)

## 2. Use (应用)

### 2.1. Advantage (优点)

* 算法可以自由切换;
* 避免使用多重条件判断;
* 扩展性良好, 符合OCP原则

### 2.2. Defect (缺点)

* 策略类数量增多;
* 所有策略类都需要对外暴露, 不符合迪米特法则(LoD).
    * 用其他模式类修正: 如工厂方法模式, 代理模式, 亨元模式等

### 2.3. Use Scenario (使用场景) 

* 多个类只有在算法或行为上稍有不同场景;
* 算法需要自由切换的场景;
* 需要屏蔽算法规则的场景;
    * 如太多的算法, 只需要知道名字即可, 传入标识, 返回运算结果.

### 3 Extension (扩展)

* 案例: 三个参数(int a,int b, String "+") 或 (int a,int b, String "-"), 进行加减法运算

* 策略枚举
    * 是一个枚举;
    * 浓缩了策略模式的枚举.
    * 由于受到枚举类型的限制(public final static ), 因此扩展性受到一定限制, 项目中一般担当不经常发生变化的角色.

### 4. Best Practice (最佳实践)

* 非常简单
* 致命缺陷: 所有策略都需要暴露出去, 这样才方便客户端决定使用哪一个策略.
* 没有定义严格的使用场景, 只是实现了策略; 实际项目中一般通过工厂方法模式来实现策略类的声明.