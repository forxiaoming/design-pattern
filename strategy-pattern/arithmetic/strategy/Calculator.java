package arithmetic.strategy;

/**
 * 计算器接口(策略抽象类)
 */
public interface Calculator {
    public int exec(int a, int b);
}
