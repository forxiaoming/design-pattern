package arithmetic.strategy;

/**
 * 加法(具体策略1)
 */
public class Add implements Calculator {
    @Override
    public int exec(int a, int b) {
        return a + b;
    }
}
