package arithmetic.strategy.extension;

/**
 * 粗略枚举
 */
public enum CalculatorEnum {
    ADD("+"){
        @Override
        public int exec(int a, int b) {
            return a + b;
        }
    },
    SUB("-"){
        @Override
        public int exec(int a, int b) {
            return a - b;
        }
    };
    String value = "";

    // 定义成员类型
    private CalculatorEnum(String value) {
        this.value = value;
    }

    // 获取枚举成员的值
    public String getValue() {
        return this.value;
    }

    // 声明一个抽象函数
    public abstract int exec(int a, int b);
}
