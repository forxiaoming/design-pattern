package arithmetic.strategy;

import arithmetic.strategy.extension.CalculatorEnum;

/**
 * 场景类
 */
public class Client {
    public final static String ADD_SYMBOL = "+";
    public final static String SUB_SYMBOL = "-";

    public static void main(String[] args) {
        int a = 1, b = 2;
        String symbol = "-";

        System.out.println("------ 策略模式");
        // 上下文
        Context context = null;
        // 判断初始化策略
        if (symbol.equals(ADD_SYMBOL)) {
            context = new Context(new Add());
        } else if (symbol.equals(SUB_SYMBOL)) {
            context = new Context(new Sub());
        }

        System.out.println("-结果: " + a + symbol + b + "=" + context.exec(a, b, symbol));

        System.out.println("------ 策略模式 扩展 - 枚举策略");

        System.out.println("-结果:  a + b = " + CalculatorEnum.ADD.exec(a, b));
        System.out.println("-结果:  a - b = " + CalculatorEnum.SUB.exec(a, b));

    }
}
