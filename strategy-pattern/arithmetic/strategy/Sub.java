package arithmetic.strategy;

/**
 * 减法(具体策略2)
 */
public class Sub implements Calculator {
    @Override
    public int exec(int a, int b) {
        return a - b;
    }
}
