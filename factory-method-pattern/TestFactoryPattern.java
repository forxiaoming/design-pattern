import base.factory.demo.ConcreteCreator;
import base.factory.demo.ConcreteProduct1;
import base.factory.demo.ConcreteProduct2;
import base.factory.demo.Product;

/**
 * 场景类
 */
public class TestFactoryPattern {

    public static void main(String[] args) {

        ConcreteCreator factory = new ConcreteCreator();
        Product product1 = factory.createProduct(ConcreteProduct1.class);
        product1.method();
        product1.method2();

        Product product2 = factory.createProduct(ConcreteProduct2.class);
        product2.method();
        product2.method2();
    }
}

