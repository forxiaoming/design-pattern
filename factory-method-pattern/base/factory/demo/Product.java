package base.factory.demo;

/**
 * 抽象产品类
 */
public abstract class Product {
    // 产品类公共方法
    public void method() {
        System.out.println("产品公共方法");
    }

    // 抽象方法
    public abstract void method2();
}
