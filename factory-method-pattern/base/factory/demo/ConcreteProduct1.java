package base.factory.demo;

/**
 * 产品具体实现 1
 */
public class ConcreteProduct1 extends Product {

    @Override
    public void method2() {
        System.out.println("具体实现类1的抽象方法实现");
    }
}
