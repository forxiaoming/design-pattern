package base.factory.demo;

/**
 * 具体工厂类
 */
public class ConcreteCreator extends CreatorFactory {
    @Override
    public <T extends Product> T createProduct(Class<T> c) {
        Product product = null;

        try {
            /**
             * 这里使用newInstance() , 而不是使用new 关键字？
             */
            product = (Product) Class.forName(c.getName()).newInstance();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return (T) product;
    }
}
