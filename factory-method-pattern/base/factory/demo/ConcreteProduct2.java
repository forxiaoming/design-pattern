package base.factory.demo;

/**
 * 产品具体实现2
 */
public class ConcreteProduct2 extends Product {
    @Override
    public void method2() {
        System.out.println("具体实现类2的抽象方法实现");
    }
}
