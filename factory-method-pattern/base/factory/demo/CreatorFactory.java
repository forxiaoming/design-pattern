package base.factory.demo;

/**
 * 抽象工厂类
 */
public abstract class CreatorFactory {
    /**
     * 创建一个产品对象， 其输入参数类型可以自行设置
     * 通常为String Enum Class 等， 也可以为空
     */
    public abstract <T extends Product> T createProduct(Class<T> c);
}
