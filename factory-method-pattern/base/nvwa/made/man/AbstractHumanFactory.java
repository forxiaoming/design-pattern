package base.nvwa.made.man;

/**
 * 抽象的八卦炉
 */
public abstract class AbstractHumanFactory {
    public abstract  <T extends Human> T createHuman( Class<T> c);
}
