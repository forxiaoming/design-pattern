package base.nvwa.made.man;

public interface Human {

    public void getColor();

    public void talk();
}
