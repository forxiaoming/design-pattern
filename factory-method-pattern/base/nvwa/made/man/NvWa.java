package base.nvwa.made.man;

/**
 * 女娲
 * (基础的工厂方法模式)
 */
public class NvWa {
    public static void main(String[] args) {
        // 声明八卦炉
        AbstractHumanFactory humanFactory = new HumanFactory();

        Human blackHuman = humanFactory.createHuman(BlackHuman.class);
        blackHuman.getColor();
        blackHuman.talk();

        Human whiteHuman = humanFactory.createHuman(WhiteHuman.class);
        whiteHuman.getColor();
        whiteHuman.talk();


        Human yellowHuman = humanFactory.createHuman(YellowHuman.class);
        yellowHuman.getColor();
        yellowHuman.talk();


    }
}
