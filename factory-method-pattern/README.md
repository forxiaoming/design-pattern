## Factory Method Pattern （工厂方法模式）

### 1. Whta ?

定义一个用于创建对象的接口， 让子类决定实例化哪一个类

工厂方法使一个类的实例化延迟到其子类

```demo01```

### 2. 优缺点 ?
* 2.1 良好的封装性, 代码结构清晰;
* 2.3 屏蔽产品类; 调用者不需要关心实现类如何变化, 只需要关心接口的变化
* 2.4 典型的解藕框架; ( 符合迪米特法则,依赖倒置原则,

应用： JDBC , 数据库从MySQL 切换到 Oracle, 只需要修改驱动名称

### 3. Then extend of Factory Method Pattern 

#### 3.1 Simple Factory Pattern (简单工厂模式)

也叫静态工厂模式, 去除了工厂的抽象, 并提供一个静态方法来创建对象

[simple factory pattern](./extend/simple/factory/pattern)

#### 3.2 More Factory Pattern （多个工厂类）

多个工厂类, 为每个产品类别提供一个工厂

每个产品类都已经明确自己的职责, 不需要传递参数了

[more factory pattern](./extend/more/factory/pattern)

#### 3.3 Replace Singleton Pattern (替代单例模式)
构造器设置访问权限, 通过获得类构造器生成一个对象, 然后对外提供访问, 保证内存中的对象唯一.

[replace singleton pattern](./extend/replace/singleton/pattern)
 
#### 3.4 Lazy inintialization (延迟初始化)
一个类被使用完毕, 并不被立即释放, 工厂类保持其初始状态, 等待被再次使用.

定义一个Map容器, 容纳所有产生的对象.

[lazy inintialization](./extend/alzy/inintialization)
