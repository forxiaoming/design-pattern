package extend.simple.factory.pattern;

/**
 * 女娲
 * (简单的工厂方法模式)
 * 去除了工厂的抽象, 并将提供一个静态方法来创建对象
 */
public class NvWa {
    public static void main(String[] args) {

        Human blackHuman = HumanFactory.createHuman(BlackHuman.class);
        blackHuman.getColor();
        blackHuman.talk();

        Human whiteHuman = HumanFactory.createHuman(WhiteHuman.class);
        whiteHuman.getColor();
        whiteHuman.talk();


        Human yellowHuman = HumanFactory.createHuman(YellowHuman.class);
        yellowHuman.getColor();
        yellowHuman.talk();


    }
}
