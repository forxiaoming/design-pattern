package extend.simple.factory.pattern;

public class WhiteHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("白色人种白色皮肤");
    }

    @Override
    public void talk() {
        System.out.println("白色人种的语言说话");
    }
}
