package extend.simple.factory.pattern;

public interface Human {

    public void getColor();

    public void talk();
}
