package extend.replace.singleton.pattern;

public class Singleton {
    // 不允许通过new 产生对象
    private Singleton() {
    }
    public void doSomething() {
        System.out.println("Singleton do something");
    }
}
