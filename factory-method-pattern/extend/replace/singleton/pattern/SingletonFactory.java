package extend.replace.singleton.pattern;

import java.lang.reflect.Constructor;

public class SingletonFactory {
    private static Singleton singleton;
    static {
        try {
            Class c1 = Class.forName(Singleton.class.getName());
            // 获取无参构造
            Constructor constructor = c1.getDeclaredConstructor();
            // 设置无参构造可以访问
            constructor.setAccessible(true);
            // 产生实例对象
            singleton = (Singleton) constructor.newInstance();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
