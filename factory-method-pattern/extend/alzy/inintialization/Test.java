package extend.alzy.inintialization;

// 测试延迟初始化
public class Test {
    public static void main(String[] args) {
        ProductFactory factory = new ProductFactory();
        Product product1 = factory.createProduct("Product1");
        Product product11 = factory.createProduct("Product1");

        Product product2 = factory.createProduct("Product2");
        Product product21 = factory.createProduct("Product2");


    }
}
