package extend.alzy.inintialization;

import java.util.HashMap;
import java.util.Map;

public class ProductFactory {
    public static final Map<String, Product> productMap = new HashMap<>();

    public static synchronized Product createProduct(String type) {
        Product product = null;

        // 如果map中存在这个对象
        if (productMap.containsKey(type)) {
            System.out.println("容器中中存在: "+ type);
            product = productMap.get(type);
        } else {
            if (type.equals("Product1")) {
                System.out.println("new Product1");
                product = new ConcreteProduct1();
            } else {
                System.out.println("new Product2");
                product = new ConcreteProduct2();
            }
            // 把对象放到容器中
            productMap.put(type, product);

        }

        return product;
    }

}
