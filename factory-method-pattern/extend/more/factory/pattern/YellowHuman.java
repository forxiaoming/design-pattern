package extend.more.factory.pattern;

public class YellowHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("黄色人种黄色皮肤");
    }

    @Override
    public void talk() {
        System.out.println("黄色人种的语言说话");
    }
}
