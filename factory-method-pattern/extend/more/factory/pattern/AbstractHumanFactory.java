package extend.more.factory.pattern;

/**
 * 多工厂模式抽象工厂类
 */
public abstract class AbstractHumanFactory {
    // 每个产品类都已经明确自己的职责, 不需要传递参数了
    public abstract Human createHuman();
}
