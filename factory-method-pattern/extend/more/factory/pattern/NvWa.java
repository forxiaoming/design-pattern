package extend.more.factory.pattern;

/**
 * 女娲
 * (多个工厂方法模式)
 * 每个产品类都已经明确自己的职责, 不需要传递参数了
 */
public class NvWa {
    public static void main(String[] args) {

        Human blackHuman = (new BlackHumanFactory()).createHuman();
        blackHuman.getColor();
        blackHuman.talk();

        Human whiteHuman = (new WhiteHumanFactory()).createHuman();
        whiteHuman.getColor();
        whiteHuman.talk();


        Human yellowHuman = (new YellowHumanFactory()).createHuman();
        yellowHuman.getColor();
        yellowHuman.talk();


    }
}
