package extend.more.factory.pattern;

public class BlackHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("黑色人种黑色皮肤");
    }

    @Override
    public void talk() {
        System.out.println("黑色人种的语言说话");
    }
}
