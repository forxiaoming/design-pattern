package extend.more.factory.pattern;

public interface Human {

    public void getColor();

    public void talk();
}
