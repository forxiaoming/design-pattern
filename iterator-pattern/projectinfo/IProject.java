package projectinfo;

/**
 * 项目接口
 */
public interface IProject {
    String getProjectInfo();
}
