package projectinfo.iterator;

/**
 * 项目信息接口
 * (抽象容器)
 */
public interface IProject {
    String getProjectInfo();

    void add(String name, int num, int cost);

    IProjectIterator iterator();
}
