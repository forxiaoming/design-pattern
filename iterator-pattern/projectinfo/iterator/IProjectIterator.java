package projectinfo.iterator;

import java.util.Iterator;

/**
 * 项目迭代器接口
 * 抽象迭代器
 */
public interface IProjectIterator extends Iterator {
}
