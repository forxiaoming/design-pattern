package projectinfo.iterator;

import java.util.ArrayList;

/**
 * 项目迭代器
 * (具体迭代器)
 */
public class ProjectIterator implements IProjectIterator {

    private ArrayList<IProject> projectArrayList = new ArrayList<IProject>();
    private int currentItem = 0;

    public ProjectIterator(ArrayList projects) {
        this.projectArrayList = projects;
    }

    /**
     * 判断是否还有元素, 必须实现
     */
    @Override
    public boolean hasNext() {
        boolean isHave = true;
        if (this.currentItem >= projectArrayList.size()
                || this.projectArrayList.get(this.currentItem) == null) {
            isHave = false;
        }
        return isHave;
    }

    /**
     * 获取下一个值
     */
    @Override
    public IProject next() {
        return this.projectArrayList.get(this.currentItem++);
    }

}
