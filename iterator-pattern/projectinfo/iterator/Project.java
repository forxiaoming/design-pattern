package projectinfo.iterator;

import java.util.ArrayList;

/**
 * 项目信息
 * (具体容器)
 */
public class Project implements IProject {
    private ArrayList<IProject> projects = new ArrayList<>();

    // 项目人数
    private String name = "";
    // 项目人数
    private int num = 0;
    // 项目花费
    private int cost = 0;

    public Project() {
    }

    public Project(String name, int num, int cost) {
        this.name = name;
        this.num = num;
        this.cost = cost;
    }

    @Override
    public String getProjectInfo() {
        StringBuilder info = new StringBuilder();
        info.append("项目名称:");
        info.append(this.name);
        info.append("\t项目人数:");
        info.append(this.num);
        info.append("\t项目花费:");
        info.append(this.cost);

        return info.toString();
    }

    @Override
    public void add(String name,int num, int cost) {
        this.projects.add(new Project(name, num, cost));
    }

    @Override
    public IProjectIterator iterator() {
        return new ProjectIterator(this.projects);
    }
}
