package projectinfo.iterator;

/**
 * Boss 增加迭代接口 (场景类)
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("增加迭代接口");

        // 声明容器
        IProject project = new Project();
        // 放入对象数据
        for (int i = 1; i < 101; i++) {
            project.add("第" + i + "个项目", 5 * i, 1000 * i);
        }

        // 创建迭代器
        IProjectIterator projectIterator = project.iterator();
        while (projectIterator.hasNext()) {
            Project project1 = (Project) projectIterator.next();
            System.out.println(project1.getProjectInfo());
        }

    }
}
