package projectinfo;

import java.util.ArrayList;

/**
 * Boss  (场景类)
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("定义一个List , 存放所有项目对象");
        ArrayList<IProject> projects = new ArrayList<>();
        projects.add(new Project("星球大战", 2, 10000));
        for (int i = 1; i < 101; i++) {
            projects.add(new Project("第" + i + "个项目", 5 * i, i * 10000));
        }
        for (IProject project : projects) {
            System.out.println("--" + project.getProjectInfo());
        }

    }
}
