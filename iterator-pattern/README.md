# Iterator Pattern (迭代器模式)

## 0. Boss查看项目信息

* ![./img/20-1.png](./img/20-1.png)
* ![./img/20-2.png](./img/20-2.png)


## 1. What ?

* Provide a way to access the element of an aggregate object sequentially
without exposing its underlying representation.
(它提供一种方法访问一个容器对象中的各个元素, 而又不需暴露该对象的内部细节)

* 角色:
    * Iterator 抽象迭代器: 负责定义访问和遍历元素;
    * Concreteiterator 具体迭代器
    * Aggregate 抽象容器" 负责提供创建具迭代器的接口, 必然提供类似createIterator()方法, Java中一般为iterator();
    * Concrete Aggregate 具体容器
    
### 2.4  Application (应用)

* @since 1.2 java.util.Iterator
* @since 1.5 java.util.Iterable
* Java已经把迭代器模式融入基本API中，我们再去写就显得多余了。

### 3. Best Practice (最佳实践)

* 对于Java开发, 尽量不要自己写迭代器模式, 使用JDK提供的Iterator接口基本足够.