package builder01.builder.service.impl;

import builder01.builder.service.CalBuilder;
import builder01.model.service.CarModel;
import builder01.model.service.impl.BMWModel;

import java.util.ArrayList;


public class BMWBuilder extends CalBuilder {
    private BMWModel bmwModel = new BMWModel();

    @Override
    public void setSequence(ArrayList<String> sequence) {
        this.bmwModel.setSequare(sequence);
    }

    @Override
    public CarModel getCarModel() {
        return this.bmwModel;
    }
}
