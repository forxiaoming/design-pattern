package builder01.builder.service;

import builder01.model.service.CarModel;

import java.util.ArrayList;

public abstract class CalBuilder {
    public abstract void setSequence(ArrayList<String> sequence);

    public abstract CarModel getCarModel();
}