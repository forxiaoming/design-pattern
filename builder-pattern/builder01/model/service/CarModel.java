package builder01.model.service;

import java.util.ArrayList;

public abstract class CarModel {
    private ArrayList<String> sequare = new ArrayList<String>();

    protected abstract void start();

    protected abstract void stop();

    protected abstract void alarm();

    protected abstract void engineBoom();

    final public void run() {
        for (int i = 0; i < this.sequare.size(); i++) {
            switch (this.sequare.get(i)) {
                case "start":
                    this.start();
                    break;
                case "stop":
                    this.stop();
                    break;
                case "alarm":
                    this.alarm();
                    break;
                case "engine boom":
                    this.engineBoom();
            }
        }
    }

    public void setSequare(ArrayList<String> sequare) {
        this.sequare = sequare;
    }
}



