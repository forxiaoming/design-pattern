
## Builder Pattern (建造者模式)

### 0. base model
**需求:** 汽车动作的顺序可以随意调整

每个车都是一个产品, 在 CarModel 中定义一个setSequence(), 
车辆执行的动作顺序在ArrayList中, 然后run()根据sequence定义的顺序完成指定的动作.

[base](./base)

**需求增加:** 汽车的动作要国家随意, 顺序, 有无某些动作等等

引入建造者 [builder01](./builder01)

**需求再增加: ** 需求无底洞, 无理性

----

**Tip:**
ArrayList 和 HashMap 如果是定义位类的成员变量, 在方法中调用一定要先调用 clear()方法, 防止数据混乱.

----
引入导演类 director [builder02](./builder02)



### 1. What ?

Separate the construction of a compler object from its representation so that
the same construction process can create different  representations.
(将一个复杂对象的构建与它的表示分离, 是的同样的构建过程可以创建不同的表示)

#### 1.1. Role

* Proudct 产品类

    通常是实现类模板方法模式.
    
* Builder 抽象建造者

    规范产品的组建, 一般由子类实现.
    
* ConcreteBuilder 具体建造者

    实现抽象类定义的所有方法.    
 
* Director 导演类

    负责安排已有模块的顺序.
    
### 2. Use

#### 2.1. Advantage ?

* 封装性: 如CarModel;
* 建造者独立, 容易扩展: 如CalBuilder的子类;
* 便于控制细节风险: 实现细节的建造者都是独立的;

#### 2.2. Use scene ?

* 相同方法不同顺序不同结果;
* 多个部件专配到一个对象, 但产生结果不同;
* 产品类复杂且多;

### 3. Best Practice

在使用建造者模式会的时候考虑一下模板方法模式, 别孤立思考一个模式
                            