package base.service.impl;

import base.service.CarModel;

/**
 * @author xiaoming
 * @version 1.0
 * @date 2019-10-17 20:36
 **/
public class BenzModel extends CarModel {
    @Override
    protected void start() {
        System.out.println("Benz start ...");
    }

    @Override
    protected void stop() {
        System.out.println("Benz stop");
    }

    @Override
    protected void alarm() {
        System.out.println("Benz alarm ...");
    }

    @Override
    protected void engineBoom() {
        System.out.println("benz engine boom ...");
    }
}
