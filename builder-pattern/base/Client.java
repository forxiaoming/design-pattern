package base;

import base.service.impl.BenzModel;

import java.util.ArrayList;

public class Client {
    public static void main(String[] args) {
        ArrayList<String> sequare = new ArrayList<>();
        sequare.add("start");
        sequare.add("alarm");
        sequare.add("stop");
        sequare.add("engine boom");

        BenzModel benzModel = new BenzModel();
        benzModel.setSequare(sequare);
        benzModel.run();
    }
}
