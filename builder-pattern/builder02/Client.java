package builder02;

import builder01.builder.service.impl.BMWBuilder;
import builder01.builder.service.impl.BenzBuilder;
import builder01.model.service.impl.BMWModel;
import builder01.model.service.impl.BenzModel;

import java.util.ArrayList;
/*
* 引入建造者
* */
public class Client {
    public static void main(String[] args) {
        ArrayList<String> sequare = new ArrayList<>();
        sequare.add("start");
        sequare.add("alarm");
        sequare.add("stop");
        sequare.add("engine boom");

//        BenzModel benzModel = new BenzModel();
//        benzModel.setSequare(sequare);
        BenzBuilder benzBuilder = new BenzBuilder();
        benzBuilder.setSequence(sequare);

        BenzModel benzModel = (BenzModel) benzBuilder.getCarModel();
        benzModel.run();

        BMWBuilder bmwBuilder = new BMWBuilder();
        bmwBuilder.setSequence(sequare);

        BMWModel bmwModel = (BMWModel) bmwBuilder.getCarModel();
        bmwModel.run();

    }
}
