package builder02.builder.service.impl;

import builder02.builder.service.CalBuilder;
import builder02.model.service.CarModel;
import builder02.model.service.impl.BenzModel;

import java.util.ArrayList;

public class BenzBuilder extends CalBuilder {
    private BenzModel benzModel = new BenzModel();

    @Override
    public void setSequence(ArrayList<String> sequence) {
        this.benzModel.setSequare(sequence);
    }

    @Override
    public CarModel getCarModel() {
        return benzModel;
    }
}
