package builder02;

import builder02.builder.service.impl.BMWBuilder;
import builder02.builder.service.impl.BenzBuilder;
import builder02.model.service.impl.BMWModel;
import builder02.model.service.impl.BenzModel;

import java.util.ArrayList;

/**
 * 导演类
 */
public class Director {
    private ArrayList<String> sequence = new ArrayList<>();
    private BenzBuilder benzBuilder = new BenzBuilder();
    private BMWBuilder bmwBuilder = new BMWBuilder();

    /**
     * Type A BenzModel, start first, then stop
     */
    public BenzModel getABenzModel() {
        // Attention: clear sequence
        this.sequence.clear();

        this.sequence.add("start");
        this.sequence.add("stop");

        this.benzBuilder.setSequence(sequence);
        return (BenzModel) this.benzBuilder.getCarModel();
    }

    /**
     * Type B BenzModel, engine boom first, then start , then stop
     */
    public BenzModel getBBenzModel() {
        this.sequence.clear();

        this.sequence.add("engine boom");
        this.sequence.add("start");
        this.sequence.add("stop");

        this.benzBuilder.setSequence(sequence);
        return (BenzModel) this.benzBuilder.getCarModel();
    }

    /**
     * Type C BMWModel, alarm first, then start, then stop
     */
    public BMWModel getCBMWModel() {
        this.sequence.clear();

        this.sequence.add("alarm");
        this.sequence.add("start");
        this.sequence.add("stop");

        this.bmwBuilder.setSequence(sequence);
        return (BMWModel) this.bmwBuilder.getCarModel();
    }
}
