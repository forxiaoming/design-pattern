# Observer Pattern (观察者模式)

## 1. What

观察者模式（Observer Pattern）也叫做发布订阅模式（Publish/subscribe）,
它是一个在项目中经常使用的模式，其定义如下：

Define a one-to-many dependency between objects so that when one object changes state,
all its dependents are notified and updated automatically.
（定义对象间一种一对多的依赖关系，使得每当一个对象改变状态，则所有依赖于它的对象都会得到通知并被自动更新。）”

### 角色

* Subject: 目标（被观察者）,能够动态增减观察者，管理观察者并通知观察者
* ConcreteSubject: 具体目标，定义被观察者自己的业务逻辑，同时定义对哪些事件进行通知
* Observer: 观察者，接到通知后执行更新操作，对信息进行处理
* ConcreteObserver: 具体观察者，定义具体的观察者处理逻辑

UML类图：

![UML类图](./img/1.png)



## 2. Use

### 2.1. Advantage ?

* 观察者和被观察者之间抽象耦合
* 建立一套触发机制 

### 2.2. Defect ?

* 开发效率及执行效率问题

### 2.3. Use scenario (使用场景) ?

* 关联行为场景。需要注意的是，关联行为是可拆分的，而不是“组合”关系
* 事件多级触发
* 跨系统消息交换场景：如消息队列机制

### 2.4 Extension (扩展) ?

JDK中提供了:java.util.Observable实现类和java.util.Observer接口

![java 中的观察者模式](./img/2.png)


### 3. 最佳实践

* 文件系统
* 猫鼠游戏
* ATM取钱
* 广播收音机