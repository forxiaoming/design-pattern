package demo;

import java.util.Observable;

public class ClientMain {
    public static void main(String[] args) {
        LiSi liSi = new LiSi();
        HanFeiZi hanFeiZi = new HanFeiZi();

        hanFeiZi.addObserver(liSi);
        hanFeiZi.haveBreakfast();
        hanFeiZi.haveFun();

    }
}
