package demo;

import java.util.Observable;

public class HanFeiZi extends Observable implements IHanFeiZi {
    @Override
    public void haveBreakfast() {
        System.out.println("韩非子：开始吃饭");
        // 通知观察者
        super.setChanged();
        super.notifyObservers("韩非子在吃饭");
    }

    @Override
    public void haveFun() {
        System.out.println("韩非子：开始娱乐");
        // 通知观察者
        super.setChanged();
        super.notifyObservers("韩非子在娱乐");
    }
}
