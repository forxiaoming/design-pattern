package demo;

/**
 * 被观察者
 */
public interface IHanFeiZi {
    public void haveBreakfast();
    public void haveFun();
}
