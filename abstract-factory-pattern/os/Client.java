package os;

import os.factory.AbstractAppFactory;
import os.factory.LinuxAppFactory;
import os.factory.MacAppFactory;
import os.factory.WinAppFactory;

public class Client {
    public static void main(String[] args) {
        AbstractAppFactory linuxAppFactory = new LinuxAppFactory();
        AbstractAppFactory winAppFactory = new WinAppFactory();
        AbstractAppFactory macAppFzctory = new MacAppFactory();

        linuxAppFactory.createEdit().implementCode();
        linuxAppFactory.createEdit().edit();

        linuxAppFactory.createViewer().implementCode();
        linuxAppFactory.createViewer().viewPic();

        winAppFactory.createEdit().implementCode();
        winAppFactory.createEdit().edit();

        macAppFzctory.createViewer().implementCode();
        macAppFzctory.createViewer().viewPic();
    }
}
