package os.vo;

/**
 * 查看器
 */
public abstract class AbstractViewerApp {
    public void viewPic() {
        System.out.println("查看图片");
    }
    // 不同的实现类
    public abstract void implementCode();

}
