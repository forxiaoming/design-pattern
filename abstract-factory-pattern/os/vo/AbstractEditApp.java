package os.vo;

/**
 * 编辑器应用
 */
public abstract class AbstractEditApp {
    public void edit() {
        System.out.println("Edit File");
    }
    // 不同的实现
    public abstract void implementCode();

}
