package os.factory;

import os.vo.AbstractEditApp;
import os.vo.AbstractViewerApp;
import os.vo.LinuxEditApp;
import os.vo.LinuxViewerApp;

public class LinuxAppFactory extends AbstractAppFactory {
    @Override
    public AbstractEditApp createEdit() {
        return new LinuxEditApp();
    }

    @Override
    public AbstractViewerApp createViewer() {
        return new LinuxViewerApp();
    }
}
