package os.factory;

import os.vo.AbstractEditApp;
import os.vo.AbstractViewerApp;
import os.vo.MacEditApp;
import os.vo.MacViewerApp;

public class MacAppFactory extends AbstractAppFactory {
    @Override
    public AbstractEditApp createEdit() {
        return new MacEditApp();
    }

    @Override
    public AbstractViewerApp createViewer() {
        return new MacViewerApp();
    }
}
