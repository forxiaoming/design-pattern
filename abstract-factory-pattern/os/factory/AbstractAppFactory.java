package os.factory;

import os.vo.AbstractEditApp;
import os.vo.AbstractViewerApp;

public abstract class AbstractAppFactory {
    // 创建Edit
    public abstract AbstractEditApp createEdit();

    // 创建Viewer
    public abstract AbstractViewerApp createViewer();
}
