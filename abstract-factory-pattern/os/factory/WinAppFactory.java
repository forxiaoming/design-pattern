package os.factory;

import os.vo.AbstractEditApp;
import os.vo.AbstractViewerApp;
import os.vo.WinEditApp;
import os.vo.WinViewerApp;

public class WinAppFactory extends AbstractAppFactory {
    @Override
    public AbstractEditApp createEdit() {
        return new WinEditApp();
    }

    @Override
    public AbstractViewerApp createViewer() {
        return new WinViewerApp();
    }
}
