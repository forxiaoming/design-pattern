package demo;

import demo.factory.AbstractCreatorFactory;
import demo.factory.Creator1Factory;
import demo.factory.Creator2Factory;
import demo.vo.AbstractProductA;
import demo.vo.AbstractProductB;

public class Client {

    public static void main(String[] args) {
        // 定义两个工厂
        AbstractCreatorFactory creatorFactory1 = new Creator1Factory();
        AbstractCreatorFactory creatorFactory2 = new Creator2Factory();

        // 生产对象
        AbstractProductA productA1 = creatorFactory1.createProductA();
        AbstractProductA productA2 = creatorFactory2.createProductA();

        AbstractProductB productB1 = creatorFactory1.createProductB();
        AbstractProductB productB2 = creatorFactory2.createProductB();

        productA1.shareMethod();
        productA1.doSomething();

        productA2.shareMethod();
        productA2.doSomething();

        productB1.shareMethod();
        productB1.doSomething();

        productB2.shareMethod();
        productB2.doSomething();

    }
}
