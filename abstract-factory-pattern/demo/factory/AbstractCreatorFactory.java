package demo.factory;

import demo.vo.AbstractProductA;
import demo.vo.AbstractProductB;

public abstract class AbstractCreatorFactory {
    // 创建A产品族
    public abstract AbstractProductA createProductA();
    // 创建B产品族
    public abstract AbstractProductB createProductB();

}
