package demo.factory;

import demo.vo.AbstractProductA;
import demo.vo.AbstractProductB;
import demo.vo.ProductA1;
import demo.vo.ProductB1;

public class Creator1Factory extends AbstractCreatorFactory {
    // 只生产产品等级为1的A产品
    @Override
    public AbstractProductA createProductA() {
        return new ProductA1();
    }
    // 只生产产品等级为1的B产品
    @Override
    public AbstractProductB createProductB() {
        return new ProductB1();
    }
}
