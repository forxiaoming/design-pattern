package demo.factory;

import demo.vo.AbstractProductA;
import demo.vo.AbstractProductB;
import demo.vo.ProductA2;
import demo.vo.ProductB2;

public class Creator2Factory extends AbstractCreatorFactory {
    // 只生产产品等级为2的A的产品
    @Override
    public AbstractProductA createProductA() {
        return new ProductA2();
    }

    // 只生产产品等级为2的B的产品
    @Override
    public AbstractProductB createProductB() {
        return new ProductB2();
    }
}
