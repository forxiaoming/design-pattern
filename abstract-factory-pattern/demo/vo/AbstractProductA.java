package demo.vo;

public abstract class AbstractProductA {
    public void shareMethod() {
        System.out.println("A产品族共同的方法");
    }
    // 每个产品相同方法, 不同实现
    public abstract void doSomething();
}
