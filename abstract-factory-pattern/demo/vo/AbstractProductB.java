package demo.vo;

public abstract class AbstractProductB {
    public void shareMethod() {
        System.out.println("B产品族共同的方法");
    }
    // 每个产品相同方法, 不同实现
    public abstract void doSomething();
}
