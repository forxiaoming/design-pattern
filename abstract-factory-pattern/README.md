## Abstract Factory Pattern （抽象工厂模式）

### 0. 女娲的失误
肤色(工厂)+性别(对象)

[nvwa.made.man](./nvwa/made/man)

### 1. What ?
Provide an interface creating families of related or dependent
without specifying their concrete classes.

为创建一组相关或者相互依赖的对象提供一个接口， 而且无需指定它们的具体类 (参考女娲造人的工厂)

**注意1** : 有N个产品族, 在抽象工厂就应该有多少个创建方法 (参见AbstractCreatorFactory)
**注意2** : 有M个产品等级就应该有M个实现工厂类, 在每个实现工厂中, 实现不同产品族的生产(参见工厂类实现类)

在场景类中, 没有任何一个方法与实现类有关系, 
对于一个产品来说, 我们只要知道它的工厂方法就可以直接生产一个产品对象, 无需关心他的实现类.


[demo](./demo)

### 2. Application

#### 2.1. Advantage?
1. 封装性

产品的实现不需要高层关心, 我们只需要知道工厂类是谁就能生产需要的对象.

2. 产品族内的约束为非公开状态

产品的约束在工厂内实现

#### 2.2. Defect?

1. 产品族扩展非常困难

添加一个产品, 需要改动代码太多, 不符合开闭原则

#### 2.3. Scenes to be used

一个对象族(或者一组没有任何关系的对象)都有相同的约束

如: 产生不同操作系统下的编辑器和图片处理器, 相同的约束: 系统类型

TODO code

#### 2.4. Notice

虽然产品族扩展非常困难, 但是产品等级扩展容易, 只需要添加一个工厂实现类

### 3. Best Practice
如一个应用(产品),三个平台(等级工厂类)

























