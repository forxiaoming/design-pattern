package nvwa.made.man.factory;

import nvwa.made.man.vo.Human;

public interface HumanFactory {
    public Human createYellowHuman();

    public Human createWhiteHuman();

    public Human createBlackHuman();
}
