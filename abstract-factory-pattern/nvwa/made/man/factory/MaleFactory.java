package nvwa.made.man.factory;

import nvwa.made.man.vo.Human;
import nvwa.made.man.vo.MaleYellowHuman;

public class MaleFactory implements HumanFactory {
    @Override
    public Human createYellowHuman() {
        return new MaleYellowHuman();
    }

    @Override
    public Human createWhiteHuman() {
        return null;
    }

    @Override
    public Human createBlackHuman() {
        return null;
    }
}
