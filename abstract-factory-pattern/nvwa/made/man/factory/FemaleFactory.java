package nvwa.made.man.factory;

import nvwa.made.man.vo.Human;
import nvwa.made.man.vo.FemaleYellowHuman;

public class FemaleFactory implements HumanFactory {
    @Override
    public Human createYellowHuman() {

        return new FemaleYellowHuman();
    }

    @Override
    public Human createWhiteHuman() {
        return null;
    }

    @Override
    public Human createBlackHuman() {
        return null;
    }
}
