package nvwa.made.man.vo;

public abstract class AbstractWhiteHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("白色人种白色皮肤");
    }

    @Override
    public void talk() {
        System.out.println("白色人种说话");
    }

}
