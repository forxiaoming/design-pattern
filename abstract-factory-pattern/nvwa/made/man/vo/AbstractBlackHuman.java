package nvwa.made.man.vo;

public abstract class AbstractBlackHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("黑色人种黑色皮肤");
    }

    @Override
    public void talk() {
        System.out.println("黑色人种说话");
    }

}
