package nvwa.made.man.vo;

public abstract class AbstractYellowHuman implements Human {
    @Override
    public void getColor() {
        System.out.println("黄色人种黄色皮肤");
    }

    @Override
    public void talk() {
        System.out.println("黄色人种说话");
    }
}
