package nvwa.made.man;

import nvwa.made.man.factory.FemaleFactory;
import nvwa.made.man.factory.HumanFactory;
import nvwa.made.man.factory.MaleFactory;
import nvwa.made.man.vo.Human;

public class Test {
    public static void main(String[] args) {
        HumanFactory maleHumanFactory = new MaleFactory();

        HumanFactory femaleHumanFactory = new FemaleFactory();

        Human maleYellowHuman = maleHumanFactory.createYellowHuman();
        maleYellowHuman.getColor();
        maleYellowHuman.getSex();
        maleYellowHuman.talk();

        Human femaleYellowHuman = femaleHumanFactory.createYellowHuman();
        femaleYellowHuman.getColor();
        femaleYellowHuman.getSex();
        femaleYellowHuman.talk();

    }

}
