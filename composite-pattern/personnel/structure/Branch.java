package personnel.structure;

import java.util.ArrayList;

/**
 * 树枝实现类
 */
public class Branch extends Corp {

    ArrayList<Corp> subordinateList = new ArrayList<Corp>();

    public Branch(String name, String prosition, int salary) {
        super(name, prosition, salary);
    }


    public void addSubordinate(Corp corp) {
        this.subordinateList.add(corp);
    }

    public ArrayList<Corp> getSuboridinateInfo() {
        return this.subordinateList;
    }

}
