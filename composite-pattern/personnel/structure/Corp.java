package personnel.structure;

/**
 * 公司所有人员信息的接口类
 */
public abstract class Corp {
    private String name = "";
    private String prosition = "";
    private int salary;

    public Corp(String name, String prosition, int salary) {
        this.name = name;
        this.prosition = prosition;
        this.salary = salary;
    }

    public String getInfo() {
        StringBuilder info = new StringBuilder();
        info.append("姓名:");
        info.append(this.name);
        info.append("\t职位:");
        info.append(prosition);
        info.append("\t薪水:");
        info.append(salary);

        return info.toString();
    }
}
