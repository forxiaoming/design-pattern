package personnel.structure;

import java.util.ArrayList;

/**
 * 场景类
 */
public class Client {
    public static void main(String[] args) {
        Branch ceo = compositeCorpTree();
        System.out.println(ceo.getInfo());
        printTreeInfo(ceo);
    }

    // 把整个树组装出来
    public static Branch compositeCorpTree() {
        // CEO
        Branch root = new Branch("万经理", "总经理", 500000);

        // 三个部门经理
        Branch developDep = new Branch("刘经理", "研发部经理", 400000);
        Branch salesDep = new Branch("马经理", "销售部经理", 400000);
        Branch financeDep = new Branch("赵经理", "财务部经理", 400000);

        // 三个组长
        Branch firstDevGroup = new Branch("杨组长", "开发一组组长", 300000);
        Branch secondDevGroup = new Branch("吴组长", "开发一组组长", 300000);

        // 所有员工
        Leaf a = new Leaf("a", "开发人员", 2000);
        Leaf b = new Leaf("b", "销售人员", 2000);
        Leaf c = new Leaf("c", "财务人员", 2000);
        Leaf d = new Leaf("d", "开发人员", 2000);

        Leaf zhengLaoLiu = new Leaf("郑老刘", "副开发经理", 300000);

        // 开始组装

        root.addSubordinate(developDep);
        root.addSubordinate(salesDep);
        root.addSubordinate(financeDep);

        developDep.addSubordinate(firstDevGroup);
        developDep.addSubordinate(secondDevGroup);
        developDep.addSubordinate(zhengLaoLiu);

        firstDevGroup.addSubordinate(a);
        secondDevGroup.addSubordinate(d);

        financeDep.addSubordinate(c);

        salesDep.addSubordinate(b);

        return root;
    }


    // 遍历整棵树
    public static void printTreeInfo(Branch root) {
        ArrayList<Corp> sub = root.getSuboridinateInfo();

        for (Corp c : sub) {
            // 直接打印员工信息
            System.out.println(c.getInfo());

            // 判断是否为普通员工(没有小弟)
            if (c instanceof Leaf) {
                continue;
            }
            printTreeInfo((Branch) c);

        }
    }
}
