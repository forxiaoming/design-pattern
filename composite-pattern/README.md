# Composite Pattern (组合模式)

## 0. 公司的人事架构示例

* ![./img/21-1.png](./img/21-1.png)
* ![./img/21-5.png](./img/21-5.png)

## 1. What

* 合成模式 / 部分-整体模式(Part-Whole)
* Compose object into tree structures to represent part-whole hierarchies.
Composite lets clients treat individual objects and compositions of objects uniformly.
(将对象组合成树形结构以表示"部分-整体"的层次结构, 是的用户对单个对象和组合对象的使用 具有一致性)
* ![./img/21-6.png](./img/21-6.png)
* 角色
    * Component 抽象构件角色: 定义参加组合对象的共有方法和属性;
    * Leaf 叶子构件: 最小单位, 没有分支;
    * Composite 树枝构件;
* 是对依赖倒转原则的破坏;

## 2. Use (应用)

### 2.1. Advantage (优点)

* 高层模块调用简单: 所有节点都是Component, 局部和整体没有区别;
* 节点自由增加;

### 2.2. Defect (缺点)

* 由于直接使用了实现类, 在面向接口编程上是不恰当的, 与依赖倒置原则冲突,  限制了接口的影响范围.

### 2.3. Use Scenario (使用场景) 

* 维护和展示部分-整体关系的场景, 如树形菜单, 文件和文件夹管理;
* 从一个整体中能够独立部分模块或功能的场景.

### 2.4 Extension (扩展)

#### 2.4.1 真实的组合模式
* 关系数据库的非对象存储功能;
* 人事架构例子, 实际项目中我们会从数据库把它们读取出来, 然后展现到前台, 用for循环和递归就可以完成这个读取.

#### 2.4.2  透明的组合模式

* ![./img/21-8.png](./img/21-8.png)

* 组合模式的安全模式:  把树枝节点和树叶节点完全分开, 树枝节点又单独拥有组合的方法, 这种方法比较安全;
* 透明的组合模式: 把组合使用的方法放到抽象类中, 不管是叶子还是树枝都有相同的结构, 通过判断getChildren 的返回值确认是叶子还是树枝节点;
    * 叶子类中不支持的方法的处理: 使用`@Deprecated`注解, 并且抛出`UnsupportedOperationException`异常
    * 递归遍历树的的时候不需要强制类型转化;
    * 遵循了依赖倒置原则, 方便系统进行扩展;

#### 2.4.3 组合模式的遍历

* 树的遍历, 从上到下, 如何从下到上遍历
* 增加一个父节点对象属性;

### 3. Best Practice (最佳实践)

* 树结构