package school.report;

/**
 * 父亲查看(场景类)
 */
public class FatherClient {
    public static void main(String[] args) {
System.out.println("------ 原始成绩单");
        FouthGradeAbstractSchoolReport report = new FouthGradeAbstractSchoolReport();
        report.report();

System.out.println("------ 稍作美化");
        SugarFouthGradeAbstractSchoolReport sugarReport = new SugarFouthGradeAbstractSchoolReport();
        sugarReport.report();
        sugarReport.sign("张三");

System.out.println("------ 装饰模式改进");
        // 原装成绩单
        AbstractSchoolReport report1 = new FouthGradeAbstractSchoolReport();
        // 加了高分的成绩单(第一次修饰)
        report1 = new HighScoreDecorator(report1);
        // 又加了排名的成绩单(第二次修饰)
        report1 = new SortDecorator(report1);
        // 看成绩单
        report1.report();
        // 签字
        report1.sign("李四");
    }
}
