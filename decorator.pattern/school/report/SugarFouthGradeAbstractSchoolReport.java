package school.report;

/**
 * 修饰四年级成绩单
 */
public class SugarFouthGradeAbstractSchoolReport extends FouthGradeAbstractSchoolReport {
    // 要美化的私有方法
    private void reportHighScore() {
        System.out.println("这次语文最高成绩 75, 数学78, 自然 80");
    }

    // 要美化的私有方法
    private void reportSort() {
        System.out.println("我的排名是第38名");
    }

    // 重写汇报内容
    @Override
    public void report() {
        // 先说最高成绩
        this.reportHighScore();
        // 看成绩单
        super.report();
        // 汇报学校排名
        this.reportSort();
    }
}
