package school.report;

/**
 * 装饰角色的抽象类(特殊的代理类)
 */
public class Decorator extends AbstractSchoolReport {

    private AbstractSchoolReport report;

    // 设定成绩单
    public Decorator(AbstractSchoolReport report) {
        this.report = report;
    }

    @Override
    public void report() {
        this.report.report();
    }

    @Override
    public void sign(String name) {
        this.report.sign(name);
    }
}
