package school.report;

/**
 * 四年级成绩单
 */
public class FouthGradeAbstractSchoolReport extends AbstractSchoolReport {
    @Override
    public void report() {
        // 成绩单
        System.out.println("尊敬的xxx家长:");
        System.out.println(" 语文 62\n 数学 67 \n 体育 93 \n 自然 69");
        System.out.println("                      家长签字:       ");
    }

    @Override
    public void sign(String name) {
        System.out.println("家长签字: " + name);
    }
}
