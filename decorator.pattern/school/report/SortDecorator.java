package school.report;

/**
 * 具体的修饰
 */
public class SortDecorator extends Decorator {
    public SortDecorator(AbstractSchoolReport report) {
        super(report);
    }

    // 汇报排名情况
    private void reportSort() {
        System.out.println("我的排名是第38名");
    }

    // 先汇报排名

    @Override
    public void report() {
        this.reportSort();
        super.report();
    }
}
