package school.report;

/**
 * 成绩单
 */
public abstract class AbstractSchoolReport {
    // 展现成绩情况
    public abstract void report();

    // 家长签字
    public abstract void sign(String name);
}
