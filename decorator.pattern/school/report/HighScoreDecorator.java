package school.report;

/**
 * 具体的修饰
 */
public class HighScoreDecorator extends Decorator {
    public HighScoreDecorator(AbstractSchoolReport report) {
        super(report);
    }

    // 汇报最高成绩
    private void reportHighScore() {
        System.out.println("这次语文最高成绩 75, 数学78, 自然 80");
    }

    // 先给父亲看最高成绩
    @Override
    public void report() {
        this.reportHighScore();
        super.report();
    }
}
