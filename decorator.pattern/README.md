# Decorator Pattern (装饰模式)

## 0. 成绩单问题

![./school/report/17-1.png](./school/report/17-1.png)
![./school/report/17-2.png](./school/report/17-2.png)
![./school/report/17-3.png](./school/report/17-3.png)
![./school/report/17-4.png](./school/report/17-4.png)

## 1. What?

* Attach additional responsibilities to an object dynamically keeping the same interface.
    Decorators provide a flexible alternative to subclassing for extending functionality.
    (动态地给一个对象添加一些额外的职责. 就增加功能来说, 装饰模式相比生成子类更为灵活.) 
    
* **角色**
    * Component (抽象构件) : 最基本 最核心 最原始的接口或抽象类
    * ConcreteComponent (具体构件)
    * Decorate (装饰角色)
    * 具体装饰角色

## 2. Use (使用)

### 2.1. Advantage (优点)

* 装饰类与被装饰类可以独立发展, 不会相互耦合;
* 是继承关系的一个替代方案, is-a 关系;
* 可以动态地扩展一个实现类的功能(定义).

### 2.2. Defect (缺点)

* 多层的装饰是比较复杂的: 剥洋葱, 到最后发现是最里层的问题, 工作量增大.

### 2.3. Use scenario (使用场景) 

* 需要扩展一个类的功能, 或者附加功能;
* 需要动态给一个对象增加功能, 也可以再动态地撤销;
* 需要为一批的兄弟类进行改装或加装功能.

疫情闹了这么久, 也不见有领导发话, 特殊时期,没有带头人, 
很多人, 都不敢说话, 怕说错

### 3. Best practice (最佳实践)

* 是对继承的有力补充.
* (实际项目中需要考虑易扩展,易复用,易维护).
* 继承是静态地给类增加功能, 装饰模式则是动态地增加功能.
* 扩展性非常好: 对于一个令项目大量延迟的需求时, 通过装饰模式重新封装一个类, 而不是通过继承来完成, 
    如: Father类-->Son类-->GrandSon类 , 需要对Son类增强, 通过建立SonDecorator类来修饰Son, 相当于创建新类, 对原有程序没有变更.
