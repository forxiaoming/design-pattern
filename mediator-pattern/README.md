# Mediator Pattern (中介者模式)

## 0. 进销存管理demo

* 通过引入中介者, 让各个类只处理自己的事情, 与自己无关的就交给中介者处理.
* 取消了各个类之间的依赖, 高内聚低耦合

## 1. What ?

Define an object that encapsulates how a set of objects interact.
Mediator promotes loose coupling by keeping objects from referring to each other explicitly,
and it lets you vary their interaction independently.

(用一个中介对象封装一系列的对象交互, 中介者使各对象不需要显示地相互作用, 
从而使其耦合松散, 而且可以独立地改变它们之间的交互)

* 角色:defect
    * Mediator 抽象中介者
    * Concrete Mediator 具体中介者
    * Colleague 同事角色
        * Self-Method : 自发行为
        * Dep-Method: 依赖方法, 依赖中介者.

* 为何同事类要使用构造函数注入中介者, 而中介者使用getter/setter方法注入同事类?
    * 因为同事类必须有中介者, 而中介者却可以只有部分同事类.
## 2. Use ?

### 2.1. Advantage ?

* 减少类间的依赖.
* 降低了类间耦合.

### 2.2. Defect ?

* 中介者会膨胀得很大.
* 同事类越多, 中介者的逻辑就越复杂.

### 2.3. Use scenario (使用场景) ?

* 简单但是不容易使用, 易被误用.
* 类间必然依赖, 存在即合理.
* 并非只要有多个依赖就要考虑中介者.
* 何时考虑? 
    * 蜘蛛网结构　--> 星型结构.

### 2.4 Practical Application (实际应用) ?

* 机场调度中心
* MVC框架
* 媒体网关
* 中介服务 

### 3. Best practice

* 在如下情况可以考虑使用:
    * N个对象之间产生了相互依赖关系(N>2)
    * 多个对象有依赖关系, 但是依赖的行为尚不确定或者有发生改变的可能, 降低变更一起的风险扩散
    * 产品开发. 如MVC框架, 可提升 产品的性能和扩展性, 但是项目开发就未必, 因为项目是以交付投产为目标, 
    而产品则是以稳定、高效、扩展为宗旨.