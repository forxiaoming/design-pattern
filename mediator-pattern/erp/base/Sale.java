package erp.base;

import java.util.Random;

// 销售管理
public class Sale {
    public void sellIBMComputer(int number) {
        // 访问库存
        Stock stock = new Stock();
        // 访问采购
        Purchase purchase = new Purchase();

        if (stock.getStackNumber() < number) {
            purchase.buyIBMcomputer(number);
        }

        System.out.println("销售 IBM 电脑: " + number + "台");
        stock.decrease(number);
    }

    // 反销售情况, 0 - 100之间变化
    public int getSaleStatus() {
        Random rand = new Random(System.currentTimeMillis());
        int saleStatus = rand.nextInt(100);
        System.out.println("IBM 电脑的销售情况为:　" + saleStatus);
        return saleStatus;
    }

    // 打折处理
    public void offSale() {
        // 库房有多少卖多少
        Stock stock = new Stock();
        System.out.println("折价销售 IBM 电脑: " + stock.getStackNumber() + "台");
    }
}
