package erp.base;

// 存货管理
public class Stock {
    // 刚开始库存有100台电脑
    private static int COMPUTER_NUMBER = 100;

    // 库存增加
    public void increase(int number) {
        COMPUTER_NUMBER += number;
        System.out.println("库存数量为: " + COMPUTER_NUMBER);
    }

    // 库存减少
    public void decrease(int number) {
        COMPUTER_NUMBER -= number;
        System.out.println("库存数量为: " + COMPUTER_NUMBER);
    }

    public int getStackNumber() {
        return COMPUTER_NUMBER;
    }

    /**
     * 存货压力太大, 要通知采购人员不要采购, 销售人员要尽快销售
     */
    public void clearStock() {
        Purchase purchase = new Purchase();
        Sale sale = new Sale();
        System.out.println("清理存货数量为: " + COMPUTER_NUMBER);
        // 要求折价销售
        sale.offSale();
        // 要求采购人员不要采购
        purchase.refuseBuyIBM();
    }
}
