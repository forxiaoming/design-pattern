package erp;

import erp.base.Purchase;
import erp.base.Sale;
import erp.base.Stock;
import erp.mediator.*;

public class Client {
    public static void main(String[] args) {
//        traditionERP();
        mediatorERP();
    }

    /**
     * 没有使用中介者模式
     */
    public static void traditionERP() {
        System.out.println("------------采购人员采购电脑------------");
        Purchase purchase = new Purchase();
        purchase.buyIBMcomputer(100);

        System.out.println("------------销售人员销售电脑------------");
        Sale sale = new Sale();
        sale.sellIBMComputer(10);

        System.out.println("------------库房人员清理库存电脑------------");
        Stock stock = new Stock();
        stock.clearStock();
    }

    /**
     * 使用中介者模式
     */
    public static void mediatorERP() {
        AbstractMediator mediator = new MediatorImpl();

        System.out.println("------------采购人员采购电脑------------");
        erp.mediator.Purchase purchase = new erp.mediator.Purchase(mediator);
        purchase.buyIBMcomputer(100);

        System.out.println("------------销售人员销售电脑------------");
        erp.mediator.Sale sale = new erp.mediator.Sale(mediator);
        sale.sellIBMComputer(10);

        System.out.println("------------库房人员清理库存电脑------------");
        erp.mediator.Stock stock = new erp.mediator.Stock(mediator);
        stock.clearStock();
    }
}
