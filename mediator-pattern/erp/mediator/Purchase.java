package erp.mediator;

/**
 * 采购管理(修改后的)
 * 简化了很多, 也清晰了很多, 处理自己的职责;
 * 与外界的关系的事务处理则交给了中介者来完成
 */
public class Purchase  extends AbstactColeague{
    public Purchase(AbstractMediator _mediator) {
        super(_mediator);
    }

    // 采购 IBM 电脑
    public void buyIBMcomputer(int number) {
/*       //访问库存
        Stock stock = new Stock();

        // 访问销售
        Sale sale = new Sale();
        // 电脑的销售情况
        int saleStatus = sale.getSaleStatus();

        if (saleStatus > 80) {
            System.out.println("销售情况良好, 采购 IBM 电脑:" + number + "台");

            stock.increase(number);
        } else {
            int buyNumber = number / 2;
            System.out.println("销售情况不好, 折半采购 IBM 电脑:" + buyNumber + "台");
        }*/

        super.mediator.execute("purchase.buy", number);

    }

    // 不再采购 IBM 电脑
    public void refuseBuyIBM() {
        System.out.println("不再采购 IBM 电脑");
    }

}
