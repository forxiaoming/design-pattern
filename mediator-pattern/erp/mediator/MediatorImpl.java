package erp.mediator;

/**
 * 具体的中介者
 * 可以根据业务要求产生多个中介者
 * 每个对象只与中介者产生依赖, 与其他对象之间没有直接关系
 * private方法则处理了各个对象之间的依赖关系
 */
public class MediatorImpl extends AbstractMediator {
    @Override
    public void execute(String str, Object... objs) {
        if (str.equals("purchase.buy")) {
            // 采购电脑
            this.buyComputer((Integer) objs[0]);
        } else if (str.equals("sale.sell")) {
            // 销售电脑
            this.sellComputer((Integer) objs[0]);
        } else if (str.equals("sale.offsell")) {
            // 折价处理
            this.offSale();
        } else if (str.equals("stock.clear")) {
            // 清仓处理
            this.clearStock();
        }


    }

    // 采购电脑
    private void buyComputer(int number) {
        // 访问库存
//        Stock stock = new Stock();

        // 访问销售
//        Sale sale = new Sale();

        // 电脑的销售情况
        int saleStatus = super.sale.getSaleStatus();

        if (saleStatus > 80) {
            System.out.println("销售情况良好, 采购电脑:" + number + "台");

            super.stock.increase(number);
        } else {
            int buyNumber = number / 2;
            System.out.println("销售情况不好, 折半采购电脑:" + buyNumber + "台");
        }
    }

    // 销售电脑
    private void sellComputer(int number) {
        // 访问库存
//        Stock stock = new Stock();
        // 访问采购
//        Purchase purchase = new Purchase();

        if (super.stock.getStackNumber() < number) {
            super.purchase.buyIBMcomputer(number);
        }

        System.out.println("销售电脑: " + number + "台");
        super.stock.decrease(number);
    }

    // 折价处理
    private void offSale() {
        // 库房有多少卖多少
//        Stock stock = new Stock();

        System.out.println("折价销售电脑: " + super.stock.getStackNumber() + "台");
    }

    // 清仓处理
    private void clearStock() {
//        Purchase purchase = new Purchase();
//        Sale sale = new Sale();

        System.out.println("清理存货数量为: " + super.stock.getStackNumber());
        // 要求折价销售
        super.sale.offSale();
        // 要求采购人员不要采购
        super.purchase.refuseBuyIBM();
    }
}
