package erp.mediator;

/**
 * 抽象同事类
 * 三具体的实现类分别继承该抽象类
 */
public abstract class AbstactColeague {
    protected AbstractMediator mediator;

    public AbstactColeague(AbstractMediator _mediator) {
        this.mediator = _mediator;
    }
}
