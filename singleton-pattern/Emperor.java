import java.util.ArrayList;
import java.util.Random;

/**
 * 有上限的多例模式
 * 多个皇帝
 **/
public class Emperor {
    // 定义最多能产生的实例数量
    private static int maxNumOfEmperor = 2;
    // 每个皇帝都有名字; 使用ArrayList来容纳每个对象的私有属性
    private static ArrayList<String> nameList = new ArrayList<String>();
    // 容纳所有皇帝实例
    private static ArrayList<Emperor> emperors = new ArrayList<Emperor>();
    // 当前皇帝序号
    private static int countNumOfEmperor = 0;
    // 生产所有对象
    static {
        for (int i = 0; i < maxNumOfEmperor; i++) {
            emperors.add(new Emperor("皇帝" + (i + 1) + ""));
        }
    }
    // 道德约束, 不产生第二个皇帝
    private Emperor() { }

    private Emperor(String name) {
        nameList.add(name);
    }

    // 随机获取对象
    public static Emperor getInstance() {
        Random random = new Random();
        countNumOfEmperor = random.nextInt(maxNumOfEmperor);
        return emperors.get(countNumOfEmperor);
    }
    public String getName() {
        return nameList.get(countNumOfEmperor);
    }
    // (其他方法)
    public static void say() {
        System.out.print(nameList.get(countNumOfEmperor));
    }

}
