/**
 * 单例模式通用代码
 * (饿汉式: 饿了, 马上就要吃)
 **/
public class Singleton {
    private static final Singleton singleton = new Singleton();
    // 限制产生多个对象
    private Singleton() { }

    // 获取实例对象
    public static Singleton getInstance() {
        return singleton;
    }

    // 类中其他方法, 尽量也是static
    public static void doSomething() {

    }
}
