/**
 * 大臣类
 **/
public class Minister {
    public static void main(String[] args) {
        // 定义5个大臣
        int ministerNum = 5;
        for (int i = 0; i < ministerNum; i++) {
            Emperor emperor = Emperor.getInstance();
            System.out.println("大臣"+ i + " 参拜 " + emperor.getName());
//            Emperor.say();
        }
    }
}
