package extend.hummer;

public class Client {
    public static void main(String[] args) {
        HummerModel h1 = new HummerH1Model();
        HummerModel h2 = new HummerH2Model();

        // 钩子函数改造
        h1.run();

        ((HummerH2Model) h2).setAlarmFlag(false);

        h2.run();

        ((HummerH2Model) h2).setAlarmFlag(true);
        h2.run();
    }
}
