package extend.hummer;

public class HummerH2Model extends HummerModel {
    // 默认要响喇叭
    private boolean alarmFlag = true;

    @Override
    protected void start() {
        System.out.println("Hummer H2 start");
    }

    @Override
    protected void stop() {
        System.out.println("Hummer H2 stop");
    }

    @Override
    protected void alarm() {
        System.out.println("Hummer H2 alarm");
    }

    @Override
    protected void engineBoom() {
        System.out.println("Hummer H2 engine boom ...");
    }

    @Override
    protected boolean isAlarm() {
        return this.alarmFlag;
    }

    public void setAlarmFlag(boolean alarmFlag) {
        this.alarmFlag = alarmFlag;
    }
}
