## Template Method Pattern （模板方法模式）

### 0. HummerModel

[hummer](./hummer)

### 1. What?

Define the skeleton of an algorithm in an operation, deferring some steps to subclasses.
Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.
(定义一个操作中的算法的框架, 而将一些步骤延迟到子类中中. 使得子类可以不改变一个算法的结构即可重新定义该算法的某些特定的步骤.)

#### 1.1. 抽象模板

两种方法: 基本方法 + 模板方法(final)

#### 1.2. 具体模板

实现父类的基本方法

#### 1.3. Notice
抽象模板中的基本方法尽量protected, 符合迪米特法则；
尽量不要扩大父类中的访问权限

### 2. Use

#### 2.1. Advantage ?
* 封装不变, 扩展可变
* 提取公共, 便于维护
* 行为父控, 子类实现

#### 2.2. Defect ?
常用接口来定义属性和方法
而模板方法模式使用抽象类来定义, 子类影响父类, 项目复杂时影响阅读

#### 2.3. Scenes to be used
* 多个子类有公有方法, 且基本逻辑相同
* 模板提供核心, 子类实现细节
* 重构, 钩子函数约束行为

### 3. Extend

* 钩子函数

钩子函数改造, h1 alarm, h2 不 alarm
[extend hummber](./extend/hummer/)

### 4. Best Practice

解决了“父类怎么调用子类的方法”问题, **因为实际项目中不推荐直接调用**

父类建立框架, 子类覆写; 修改子类, 影响了父类行为的结果, 实现了"父调用子类的方法"的效果.

 