package project.development;

/**
 * 项目组抽象类 (接收者)
 */
public abstract class Group {
    /**
     * 找到组(讨论)
     */
    public abstract void find();

    /**
     * 被要求增加功能
     */
    public abstract void add();

    /**
     * 被要求删除功能
     */
    public abstract void delete();

    /**
     * 被要求修改功能
     */
    public abstract void change();

    /**
     * 被要求给出变更计划
     */
    public abstract void plan();

    /**
     * 扩展(反悔问题)
     */
    public abstract void rollBack();
}
