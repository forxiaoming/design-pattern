package project.development;

/**
 * 场景类
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("");
        Group rq = new RequirementGroup();
        rq.find();
        rq.add();
        rq.plan();
        Group rq2 = new PageGroup();
        rq2.find();
        rq2.add();
        rq2.plan();

        // 命令模式改进
        System.out.println("------命令模式改进------");
        // 1. 定义接头人
        Invoker invoker = new Invoker();
        // 2. 客户下达命令
        Command addCommand = new AddRequirementCommand();
        // 3. 接头人收到命令
        invoker.setCommand(addCommand);
        // 4. 接头人执行命令
        invoker.action();


        // 最佳实践
        System.out.println("------最佳实践------");
        Invoker2 invoker2 = new Invoker2();
        PerfectCommand command = new ConcretePerfectCommand();
        invoker2.setCommand(command);
        invoker2.action();

    }
}
