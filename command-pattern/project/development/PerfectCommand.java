package project.development;

/**
 * 完美的Command
 */
public abstract class PerfectCommand {

    // 定义要给子类的全局共享变量
    protected final Group group;

    // 实现类必须定义一个接收者
    public PerfectCommand(Group group) {
        this.group = group;
    }

    // 执行命令
    public abstract void execute();
}
