package project.development;

/**
 * 增加一项需求的命令
 */
public class AddRequirementCommand extends Command {
    @Override
    public void execute() {

        super.rg.find();
        super.rg.add();
        super.rg.change();
    }
}
