package project.development;

/**
 * 命令类
 */
public abstract class Command {
    // 把三个组都定义好, 子类可以直接使用
    protected RequirementGroup rg = new RequirementGroup();
    protected PageGroup pg = new PageGroup();
    protected CodeGroup cg = new CodeGroup();

    /**
     * 执行命令
     */
    public abstract void execute();
}
