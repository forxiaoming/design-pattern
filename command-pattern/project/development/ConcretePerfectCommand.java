package project.development;

/**
 * 具体的命令
 */
public class ConcretePerfectCommand extends PerfectCommand {
    public ConcretePerfectCommand() {
        super(new CodeGroup());
    }

    @Override
    public void execute() {
        super.group.find();
        super.group.add();
    }
}
