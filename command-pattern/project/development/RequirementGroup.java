package project.development;

/**
 * 需求组
 */
public class RequirementGroup extends Group {
    @Override
    public void find() {
        System.out.println("找到需求组");
    }

    @Override
    public void add() {
        System.out.println("被要求增加功能");
    }

    @Override
    public void delete() {
        System.out.println("被要求删除功能");
    }

    @Override
    public void change() {
        System.out.println("被要求修改功能");
    }

    @Override
    public void plan() {
        System.out.println("被要求给出变更计划");
    }

    @Override
    public void rollBack() {
        // 根据日志进行回滚
    }
}
