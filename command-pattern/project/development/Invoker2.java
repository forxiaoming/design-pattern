package project.development;

/**
 * 负责接头人(接收者角色)
 */
public class Invoker2 {

    private PerfectCommand command;

    /**
     * 客户发出命令
     */
    public void setCommand(PerfectCommand command) {
        this.command = command;
    }

    /**
     * 执行客户的命令
     */
    public void action() {
        this.command.execute();
    }
}
