# Command Pattern (命令模式)

## 0. [example]

![Project Development](./project/development/15-1.png)

![Project Development](./project/development/15-2.png)

![Project Development](./project/development/15-3.png)


## 1. What?

(高内聚的模式)
Encapsulate a request as an object, thereby letting you parameterize clients with different requests,
queue or log requests, and support undoable operations.
(将一个请求封装成一个对象, 从而让你使用不同的请求把客户端参数化, 
对请求排队或者记录请求日志, 可以提供命令的撤销和回复功能)

![Project Development](./project/development/15-4.png)

### 三个角色

* Receive 接收者 : 该角色就是干活的角色，命令传递到这里是应该被执行的，具体到我们上面的例子中就是Group的三个实现类。 (Group类)
* Command 命令角色 : 需要执行的所有命令都在这里声明
* Invoker 调用者角色 : 接收到命令，并执行命令

## 2. Use

### 2.1. Advantage ?

* 类间解耦 : 调用者与接收者之间没有任何依赖
* 可扩展性 : Command的子类非常容易扩展
* 与其他模式结合
    * 结合责任链模式实现命令族解析任务
    * 结合模板方法模式减少Command子类膨胀的问题

### 2.2. Defect ?

* Command子类膨胀的问题 ( N个 )

### 2.3. Use scenario (使用场景) ?

* 认为是命令的地方, 如:
    * GUI开发中的一个按钮点击
    * 模拟DOS命令的时候
    * 触发反馈机制的处理

### 2.4 Extension (扩展) ?

* 同一个命令中同时调用多个接收者
* 反悔问题(撤销命令动作)
    * 方法1: 结合备忘录模式还原最后状态
    * 方法2: 接受者增加一个撤回命令(根据事务日志回滚)
    ```java
      public abstract class Group {
        /**
         * 扩展(反悔问题)
         */
        public abstract void rollBack();
      }
     ```

### 3. Best practice (最佳实践)

* 问题: 在场景类中Receiver (Group类)并有暴露给Client, 而在通用类图中出现了Client对Receiver的依赖?
    * 约定的优先级别最高 
    * 因为这里封装了Receiver, 减少了高层模块(Client类)对底层模块(Receiver角色类)的依赖, 提高了系统的稳定性
