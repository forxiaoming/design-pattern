# Design Pattern
## 创建型模式

## 结构型模式

## 行为型模式


1. [design-principles (6大设计原则)](./design-principles)
2. [singleton-pattern (单例模式)](./singleton-pattern)
3. [factory-method-pattern (工厂方法模式)](./factory-method-pattern)
4. [abstract-factory-pattern (抽象工厂模式)](./abstract-factory-pattern)
5. [template-method-pattern (模板方法模式)](./template-method-pattern)
6. [builder-pattern (建造者模式)](./builder-pattern)
7. [proxy-pattern (代理模式)](./proxy-pattern)
8. [prototype-pattern (原型模式)](./prototype-pattern)
9. [mediator-pattern (中介者模式)](./mediator-pattern)
10. [command-pattern (命令模式)](./command-pattern)
11. [chain-of-responsibility-pattern (责任链模式)](./chain.of.responsibility.pattern)
12. [decorator-pattern(装饰模式)](./decorator.pattern)
13. [strategy-pattern(策略模式)](./singleton-pattern)
14. [adapter-pattern(适配器模式)](./adapter-pattern)
15. [iterator-pattern(迭代器模式)](./iterator-pattern)
16. [composite-pattern(组合模式)](./composite-pattern)
17. [observer-pattern(观察者模式)](./observer-pattern)
18. [(门面模式)]
19. [(备忘录模式)]
20. [(访问者模式)]
21. [(状态模式)]
22. [(解释器模式)]   
23. [(享元模式)]
24. [(桥梁模式)]


## 参考
 * [设计模式之禅]()
 * [看懂UML类图和时序图](https://design-patterns.readthedocs.io/zh_CN/latest/read_uml.html)
 * [图说设计模式](https://design-patterns.readthedocs.io/zh_CN/latest/index.html#)
