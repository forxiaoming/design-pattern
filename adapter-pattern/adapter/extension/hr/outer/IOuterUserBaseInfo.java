package adapter.extension.hr.outer;

import java.util.Map;

/**
 * 用户基本信息接口
 */
public interface IOuterUserBaseInfo {
    public Map getUserBaseInfo();
}
