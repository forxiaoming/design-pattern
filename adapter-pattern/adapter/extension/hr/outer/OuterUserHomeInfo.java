package adapter.extension.hr.outer;

import java.util.HashMap;
import java.util.Map;

public class OuterUserHomeInfo implements IOuterUserHomeInfo {
    @Override
    public Map getUserHomeInfo() {
        HashMap userHomeInfoMap = new HashMap();
        userHomeInfoMap.put("homeTelNumber", "010-09080701(扩展)");
        userHomeInfoMap.put("homeAddress", "北京市海淀区(扩展)");
        return userHomeInfoMap;
    }
}
