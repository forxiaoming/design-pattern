package adapter.extension.hr.outer;

import java.util.HashMap;
import java.util.Map;

public class OuterUserOfficeInfo implements IOuterUserOfficeInfo {
    @Override
    public Map getUserOfficeInfo() {
        HashMap userOfficeInfoMap = new HashMap();
        userOfficeInfoMap.put("jobPosition", "BOSS(扩展");
        userOfficeInfoMap.put("officeTelNumber", " 18601010101(扩展)");
        return userOfficeInfoMap;
    }
}
