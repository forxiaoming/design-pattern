package adapter.extension.hr.outer;

import java.util.HashMap;
import java.util.Map;

public class OuterUserBaseInfo implements IOuterUserBaseInfo {
    @Override
    public Map getUserBaseInfo() {
        HashMap baseInfoMap = new HashMap();
        baseInfoMap.put("userName", "张三(扩展)");
        baseInfoMap.put("mobileNumber", "13401010101(扩展");
        return baseInfoMap;
    }

}
