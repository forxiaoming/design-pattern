# Adapter Pattern (适配器模式)

## 0. 查看公司员工信息和劳务公司员工信息示例

![./hr/19-3.png](./hr/19-3.png)

* 提到名词
    * BO (Business Object, 业务对象)
    * 贫血对象(Thin Business Object, 不需要存储状态以及相关的信息), 在领域模型中叫做贫血领域模型
    * 充血对象(Rich Business Object), 在领域模型中叫做充血领域模型,

## 1. What

* Convert the interface of a class into another interface clients expect,
    Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.
    (建一个类的接口变换成客户端所期待的另一种接口, 从而使原本因接口不匹配而无法在一起工作的两个类南宫在一起工作.)

* 又叫变压器模式, 或包装模式;
* 通过继承进行适配, 所以叫类适配器;
* 适配器: 例如电源适配器, 现在的 Type-c转耳机插孔;

* 三个角色:
    * Target 目标角色: 已经正在正式运行的角色, 接口或抽象类;
    * Adaptee 源角色: 服役状态, 正常的类;
    * Adapter 适配器角色

![./hr/19-4.png](./hr/19-4.png)


## 2. Use (应用)

### 2.1. Advantage (优点)

* 可以让两个没有如何关系的类在一起运行;
* 增加了类的透明性: 访问Target角色, 但是其具体实现已委托给源角色, 这些对高层模块透明;
* 增加了类的复用度: 源角色可以正常使用, 目标角色也可以充当新的演员;
* 灵活性: 想用就用, 不想用就删除.

### 2.2. Use Scenario (使用场景) 

* 有机动修改一天个已经投产中的接口时.

### 2.3. Attention (注意事项)

* 不要在设计之初考虑它: 不是为了解决还处于开发阶段的问题;
* 项目一定要遵循依赖倒置原则和里氏替换原则;

### 2.4 Extension (扩展)


* 目标源有多个接口, 但是Java不支持多继承; 通过类关联来实现.
* OuterUserInfo类变成了委托服务, 把IUserInfo所需要的所有操作都委托给其他三个接口来实现.
* ![./hr/19-8.png](./hr/19-8.png)

* 这种扩展称为:  对象适配器
* ![./hr/19-9.png](./hr/19-9.png)

### 3. Best Practice (最佳实践)

* 适配器模式只是一个"补救"模式;