package hr.outer;

import java.util.Map;

/**
 * 劳动服务公司的人员信息接口
 */
public interface IOuterUser {
    public Map getUserBaseInfo();

    public Map getUserOfficeInfo();

    public Map getUserHomeInfo();
}
