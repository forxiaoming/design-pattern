package hr.outer;

import java.util.HashMap;
import java.util.Map;

/**
 * 劳动服务公司的人员信息(源角色)
 */
public class OuterUser implements IOuterUser {
    @Override
    public Map getUserBaseInfo() {
        HashMap baseInfoMap = new HashMap();
        baseInfoMap.put("userName", "张三");
        baseInfoMap.put("mobileNumber", "13401010101");
        return baseInfoMap;
    }

    @Override
    public Map getUserOfficeInfo() {
        HashMap userOfficeInfoMap = new HashMap();
        userOfficeInfoMap.put("jobPosition", "BOSS");
        userOfficeInfoMap.put("officeTelNumber", " 18601010101");
        return userOfficeInfoMap;
    }

    @Override
    public Map getUserHomeInfo() {
        HashMap userHomeInfoMap = new HashMap();
        userHomeInfoMap.put("homeTelNumber", "010-09080701");
        userHomeInfoMap.put("homeAddress", "北京市海淀区");
        return userHomeInfoMap;
    }

}
