package hr;

/**
 * 本公司员工信息接口(目标角色信息)
 */
public interface IUserInfo {
    public String getUserName();

    public String getHomeAddress();

    public String getMobileNumber();

    public String getOfficeTelNumber();

    public String getJobPosition();

    public String getHomeTelNumber();
}
