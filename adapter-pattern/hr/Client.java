package hr;

import adapter.extension.hr.OuterUserInfoExtension;
import adapter.extension.hr.outer.OuterUserBaseInfo;
import adapter.extension.hr.outer.OuterUserHomeInfo;
import adapter.extension.hr.outer.OuterUserOfficeInfo;

/**
 * 场景类
 */
public class Client {
    public static void main(String[] args) {

        System.out.println("------ 获取本公司人员信息");
        IUserInfo userInfo = new UserInfo();
        userInfo.getUserName();
        userInfo.getHomeAddress();
        userInfo.getHomeTelNumber();

        System.out.println("------ 获取劳动服务公司的员工信息(适配器模式)");
        IUserInfo outerUserInfo = new OuterUserInfo();
        outerUserInfo.getUserName();
        outerUserInfo.getHomeAddress();
        outerUserInfo.getHomeTelNumber();

        System.out.println("------ (适配器扩展)获取劳动服务公司的员工信息");

        OuterUserBaseInfo outerUserBaseInfo = new OuterUserBaseInfo();
        OuterUserHomeInfo outerUserHomeInfo = new OuterUserHomeInfo();
        OuterUserOfficeInfo outerUserOfficeInfo = new OuterUserOfficeInfo();

        OuterUserInfoExtension outerUserInfoExtension = new OuterUserInfoExtension(outerUserBaseInfo, outerUserHomeInfo, outerUserOfficeInfo);
        outerUserInfoExtension.getUserName();
        outerUserInfoExtension.getHomeAddress();
        outerUserInfoExtension.getOfficeTelNumber();

    }
}
