package OCP;

/**
 * 对书打折销售
 * 扩展来实现变化
 **/
public class OffNovelBook extends NovelBook {
    public OffNovelBook(String name, int price) {
        super(name, price);
    }

    @Override
    public int getPrice() {
        // 打折价格
        int offPrice = 0;
        // 原价
        int selfPrice = super.getPrice();

        if (selfPrice > 4000){
            offPrice = selfPrice * 90 / 100;
        } else {
            offPrice = selfPrice * 80 / 100;
        }

        return offPrice;
    }
}
