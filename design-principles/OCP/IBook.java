package OCP;

public interface IBook {
    public String getName();

    public int getPrice();
}
