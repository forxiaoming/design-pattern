package OCP;

import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.ArrayList;

// 书店卖书
public class BookStore {
    private final static ArrayList<IBook> bookList = new ArrayList<>();

    private final static ArrayList<IBook> offBookList = new ArrayList<>();

    static {
        bookList.add(new NovelBook("新射雕英雄传", 3500));
        bookList.add(new NovelBook("飞刀又见飞刀", 4500));

        offBookList.add(new OffNovelBook("新射雕英雄传", 3500));
        offBookList.add(new OffNovelBook("飞刀又见飞刀", 4500));
    }

    public static void main(String[] args) {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        currencyFormat.setMaximumFractionDigits(2);

        System.out.println("------------- 书店售书记录 -------------");
        for (IBook book : bookList) {
            System.out.println(book.getName() + " : " + currencyFormat.format(book.getPrice() / 100.0) + " 元");
        }

        System.out.println("------------- 书店打折售书记录 -------------");

        for (IBook book : offBookList) {
            System.out.println(book.getName() + " : " + currencyFormat.format(book.getPrice() / 100.0) + " 元");
        }

    }

}
