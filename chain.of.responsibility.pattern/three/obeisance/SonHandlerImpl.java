package three.obeisance;

/**
 * 儿子处理类
 */
public class SonHandlerImpl extends Handler {
    // 儿子类只处理母亲的请求
    public SonHandlerImpl() {
        super(Handler.SON_LEVEL_REQUEST);
    }

    @Override
    protected void response(IWoman woman) {
        System.out.println("------母亲发来请求------");
        System.out.println(woman.getRequest());
        System.out.println("儿子做出回应: 同意\n");
    }
}
