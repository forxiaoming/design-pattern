package three.obeisance;

import java.util.ArrayList;
import java.util.Random;

/**
 * 场景类
 */
public class Client {
    public static void main(String[] args) {
        Random random = new Random();
        ArrayList<IWoman> arrayList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            arrayList.add(new WomanImpl(random.nextInt(4), "出去逛街"));
        }
        // 定义三个请示对象
        Handler father = new FatherHandlerImpl();
        Handler husband = new HusbandHandlerImpl();
        Handler son = new SonHandlerImpl();

        // 设置请示顺序
        father.setNext(husband);
        husband.setNext(son);

        for (IWoman woman : arrayList) {
            father.handleMessage(woman);
        }
    }
}
