package three.obeisance;

/**
 * 女性类
 */
public class WomanImpl implements IWoman {

    /**
     * 描述女性的个人状况
     * 1 -- 未出嫁
     * 2 -- 出嫁
     * 3 -- 丈夫不在
     */
    private int type = 0;

    private String request = "";

    public WomanImpl(int _type, String _request) {
        this.type = _type;

        switch (this.type) {
            case 1:
                this.request = "女儿的请求是: " + _request;
                break;
            case 2:
                this.request = "妻子的请求是: " + _request;
                break;
            case 3:
                this.request = "母亲的请求是: " + _request;
        }

    }

    @Override
    public int getType() {
        return this.type;
    }

    @Override
    public String getRequest() {
        return this.request;
    }
}
