package three.obeisance;

/**
 * 父亲处理类
 */
public class FatherHandlerImpl extends Handler {
    public FatherHandlerImpl() {
        // 父亲类只处理女儿的请求
        super(Handler.FATHER_LEVEL_REQUEST);
    }

    @Override
    protected void response(IWoman woman) {
        System.out.println("------女儿向父亲请求----");
        System.out.println(woman.getRequest());
        System.out.println("父亲回应: 同意\n");
    }
}
