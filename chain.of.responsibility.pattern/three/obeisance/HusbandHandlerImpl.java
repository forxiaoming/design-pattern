package three.obeisance;

/**
 * 丈夫处理类
 */
public class HusbandHandlerImpl extends Handler {
    public HusbandHandlerImpl() {
        // 只处理妻子的请求
        super(Handler.HUSBAND_LEVEL_REQUEST);
    }

    @Override
    protected void response(IWoman woman) {
        System.out.println("-----妻子向丈夫发出请求-----");
        System.out.println(woman.getRequest());
        System.out.println("丈夫回应: 同意\n");
    }
}
