package three.obeisance;

/**
 * 抽象处理类
 *  融合了模板方法模式
 */
public  abstract class Handler {
    public final static int FATHER_LEVEL_REQUEST = 1;
    public final static int HUSBAND_LEVEL_REQUEST = 2;
    public final static int SON_LEVEL_REQUEST = 3;

    // 能处理的级别
    private int level = 0;
    // 责任传递, 下一个责任人
    private Handler nextHandler;

    // 自己能够处理的请求级别
    public Handler(int _level) {
        this.level = _level;
    }

    // 定义一个请求的处理方法
    public final void handleMessage(IWoman woman) {
        if (woman.getType() == this.level) {
            this.response(woman);
            return;
        }

        if (this.nextHandler != null) {
            this.nextHandler.handleMessage(woman);
        } else {
            System.out.println("------ 没有地方请示处理了, 按不同意处理------");
        }
    }

    // 设置下一个环节的人
    public void setNext(Handler _handler) {
        this.nextHandler = _handler;
    }

    // 处理回应请求
    protected abstract void response(IWoman woman);
}