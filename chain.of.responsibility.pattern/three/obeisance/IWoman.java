package three.obeisance;

/**
 * 女性接口类
 */
public interface IWoman {
    // 获取自己的状况
    public int getType();

    // 获取女性的请求
    public String getRequest();
}
