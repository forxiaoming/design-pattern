# Chain of Responsibility Pattern (责任链模式)

## 0. "三从"案例

* "从父-->从夫-->从子" 构成了责任链

* 职责类实现的两个职责: 定义自己能够处理的等级级别; 对请求做出回应.
* 融合了模板方法模式

[./three/obeisance/Client.java](./three/obeisance/Client.java)

![16-1.png](./three/obeisance/16-1.png)
![16-2.png](./three/obeisance/16-2.png)
![16-3.png](./three/obeisance/16-3.png)

## 1. What?

* (使多个对象都有机会处理请求, 从而避免请求的发送者和接收者之间的耦合关系.
将这些对象连成一条链, 并沿着这条链传递该请求, 直到有对象处理它为止)

![16-4.png](./three/obeisance/16-4.png)

## 2. Use (应用)

### 2.1. Advantage (优点)

* 显著优点: 将请求和处理分开, 将请求者和处理者两者解耦

### 2.2. Defect (缺点)

* 性能问题: 每个请求都是从链头遍历到链尾, 链比较长时有性能问题.
* 调试不便: 链比较长时, 由于采取了类似递归方式, 调试时链条逻辑可能比较复杂.

### 2.3. Attention(注意事项)

* 控制链节点数量, 避免出现超长链的情况.

### 3. Best practice (最佳实践)

![16-5.png](./three/obeisance/16-5.png)
