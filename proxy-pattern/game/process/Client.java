package game.process;


import game.process.service.impl.GamePlayer;

import java.time.LocalDate;
import java.time.LocalTime;

public class Client {
    public static void main(String[] args) {
        GamePlayer gamePlayer = new GamePlayer("张三");

        gamePlayer.login("张三", "123");
        System.out.println("Game start time : " + LocalDate.now() + " " + LocalTime.now());
        gamePlayer.killBoss();
        gamePlayer.upgrade();

        System.out.println("Game stop time: " + LocalDate.now() + " " + LocalTime.now());
    }

}
