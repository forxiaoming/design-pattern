package game.proxy.service.impl;

import game.proxy.service.IGamePlayer;

/**
 * 游戏代练公司类
 */
public class GamePlayerProxy implements IGamePlayer {
    private IGamePlayer gamePlayer = null;

    public GamePlayerProxy(IGamePlayer gamePlayer) {
        this.gamePlayer =  gamePlayer;
    }

    @Override
    public void login(String userName, String password) {
        System.out.println("Proxy login");
        this.gamePlayer.login(userName, password);
    }

    @Override
    public void killBoss() {
        System.out.println("Proxy kill boss ...");
        this.gamePlayer.killBoss();
    }

    @Override
    public void upgrade() {

        System.out.println("Proxy upgrade ...");
        this.gamePlayer.upgrade();
    }
}
