package game.proxy;


import game.proxy.service.IGamePlayer;
import game.proxy.service.impl.GamePlayer;
import game.proxy.service.impl.GamePlayerProxy;

import java.time.LocalTime;

public class Client {
    public static void main(String[] args) {
        IGamePlayer gamePlayer = new GamePlayer("张三");
        IGamePlayer gamePlayerProxy  = new GamePlayerProxy(gamePlayer);

        System.out.println("Game start time : " + LocalTime.now());

        gamePlayerProxy.login("张三", "123");
        gamePlayerProxy.killBoss();
        gamePlayerProxy.upgrade();

        System.out.println("Game stop time: " + LocalTime.now());
    }

}
