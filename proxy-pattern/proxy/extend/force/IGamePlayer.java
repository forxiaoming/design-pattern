package proxy.extend.force;

public interface IGamePlayer {

    public void  login(String userName, String password);

    public void killBoss();

    public void upgrade();

    // 找到自己的代理
    public IGamePlayer getProxy();
}
