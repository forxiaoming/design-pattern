package proxy.extend.force;

public class GamePlayer implements IGamePlayer {
    private String name = "";

    private IGamePlayer proxy = null;

    public GamePlayer(String _name) {
        this.name = _name;
    }

    @Override
    public void login(String userName, String password) {
        if (isProxy()) {
            System.out.println("login: " + userName + " , " + password);
        } else {
            System.out.println("请通过代理类访问");
        }
    }

    @Override
    public void killBoss() {
        if (isProxy()) {
            System.out.println("kill boss: " + this.name);
        } else {
            System.out.println("请通过代理类访问");
        }
    }

    @Override
    public void upgrade() {
        if (isProxy()) {
            System.out.println(this.name + " upgrade ...");
        } else {
            System.out.println("请通过代理类访问");
        }

    }

    @Override
    public IGamePlayer getProxy() {
        this.proxy = new GamePlayerProxy(this);
        return this.proxy;
    }


    private boolean isProxy() {
        if (this.proxy == null) {
            return false;
        } else {
            return true;
        }
    }
}
