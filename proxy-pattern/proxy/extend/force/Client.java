package proxy.extend.force;

import java.time.LocalTime;

/**
 * 强制代理
 */
public class Client {

    public static void main(String[] args) {
        IGamePlayer gamePlayer = new GamePlayer("张三");
        //        GamePlayerProxy proxy = new GamePlayerProxy("ming");

        // 从真实角色查找代理类
        IGamePlayer proxy = gamePlayer.getProxy();

        System.out.println("Game start time: " + LocalTime.now());

        proxy.login("mingming", "123456");
        proxy.killBoss();
        proxy.upgrade();

        System.out.println("Game stop time: " + LocalTime.now());

    }
}

