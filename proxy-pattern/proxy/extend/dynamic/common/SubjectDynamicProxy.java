package proxy.extend.dynamic.common;

import java.lang.reflect.InvocationHandler;

/**
 * DynamicProxy 是一个通用类, 不具有业务意义
 * 这里产生一个 具体业务的动态代理
 **/
public class SubjectDynamicProxy extends DynamicProxy {

    public static <T> T newProxyInstance(Subject subject) {


        ClassLoader loader = subject.getClass().getClassLoader();

        Class<?>[] classes = subject.getClass().getInterfaces();

        InvocationHandler handler = new MyInvocationHandler(subject);

        return newProxyInstance(loader, classes, handler);
    }
}


