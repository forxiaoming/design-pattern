package proxy.extend.dynamic.common;

import java.lang.reflect.InvocationHandler;

public class Client {
    public static void main(String[] args) {
        // 动态代理通用代理模型
//        dynamicCommon();

        // 具体业务代理的扩展
        dynamicCommonExtend();
    }

    public static void dynamicCommon() {

        // 定义主题
        Subject subject = new RealSubject();

        // 定义Handler
        InvocationHandler handler = new MyInvocationHandler(subject);

        // 定义主题的代理
        Subject proxy = DynamicProxy.newProxyInstance(
                subject.getClass().getClassLoader(),
                subject.getClass().getInterfaces(),
                handler);

        // 代理的行为
        proxy.doSomething("Dynamic Proxy (common)");

    }

    public static void dynamicCommonExtend() {
        // 定义主题
        Subject subject = new RealSubject();

        // 定义具体的代理

        Subject proxy = SubjectDynamicProxy.newProxyInstance(subject);
        proxy.doSomething("extend dynamic proxy");
    }
}
