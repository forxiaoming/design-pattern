package proxy.extend.dynamic.common;

public interface Advice {
    // 通知只有一个方法, 执行即可
    public void exec();
}
