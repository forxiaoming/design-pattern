package proxy.extend.dynamic.common;

/**
 * 抽象主题
 */
public interface Subject {

    /**
     *业务操作
     */
    public void doSomething(String str);
}
