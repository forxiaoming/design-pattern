package proxy.extend.dynamic.common;

public class BeforeAdvice implements Advice {
    @Override
    public void exec() {
        System.out.println("I'm beforeAdvice");
    }
}
