package proxy.extend.dynamic.common;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 动态代理的Handler类
 **/
public class MyInvocationHandler implements InvocationHandler {
    /**
     * 被代理对象
     */
    private Object target = null;

    public MyInvocationHandler(Object _obj) {
        this.target = _obj;
    }

    /**
     * 代理方法
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 执行被代理的方法
        return method.invoke(this.target, args);
    }
}
