package proxy.extend.dynamic.common;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class DynamicProxy {

    public static <T> T newProxyInstance(ClassLoader loader,
                                         Class<?>[] interfaces,
                                         InvocationHandler handler) {
        // 寻找 JoinPoint(连接点), AOP 框架使用元数据定义
        if (true) {
            (new BeforeAdvice()).exec();
        }

        // 执行目标
        return (T) Proxy.newProxyInstance(loader, interfaces, handler);
    }
}
