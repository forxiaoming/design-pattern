package proxy.extend.dynamic.common;

public class RealSubject implements Subject {
    @Override
    public void doSomething(String str) {
        System.out.println("Do something : " + str);
    }
}
