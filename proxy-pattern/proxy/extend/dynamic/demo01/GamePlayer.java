package proxy.extend.dynamic.demo01;

public class GamePlayer implements IGamePlayer {
    private String name = "";

    public GamePlayer(String name) {
        this.name = name;
    }

    @Override
    public void login(String userName, String password) {
        System.out.println("用户: " + userName + " 登录成功");
    }

    @Override
    public void killBoss() {
        System.out.println(this.name + " kill boss ...");
    }

    @Override
    public void upgrade() {
        System.out.println(this.name + " upgrade ...");
    }
}
