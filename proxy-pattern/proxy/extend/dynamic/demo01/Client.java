package proxy.extend.dynamic.demo01;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.time.LocalTime;

/**
 **/
public class Client {
    public static void main(String[] args) {
        // 定义一个玩家
        IGamePlayer gamePlayer = new GamePlayer("张三");
        // 定义Handle
        InvocationHandler handler = new GamePlayIH(gamePlayer);

        System.out.println("Start time: " + LocalTime.now());

        // 获取ClassLoader
        ClassLoader gamePlayerClassLoader = gamePlayer.getClass().getClassLoader();

        // 产生一个动态代理
        IGamePlayer proxy = (IGamePlayer) Proxy.newProxyInstance(
                gamePlayerClassLoader,
                new Class[]{IGamePlayer.class}
                , handler);

        proxy.login("ming", "123");
        proxy.killBoss();
        proxy.upgrade();

        System.out.println("Stop time: " + LocalTime.now());
    }
}
