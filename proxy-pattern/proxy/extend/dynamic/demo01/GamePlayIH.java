package proxy.extend.dynamic.demo01;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class GamePlayIH implements InvocationHandler {
    /**
     * 被代理者
     */
    Class cls = null;
    /**
     * 被代理的实例
     */
    Object object = null;
    /**
     * 我要代理谁
     */
    public GamePlayIH(Object object) {
        this.object = object;
    }

    /**
     * 调用被代理的方法
     * @param proxy
     * @param method
     * @param args
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        // 反射调用方法
        if ("LOGIN".equalsIgnoreCase(method.getName())) {
            System.out.println("--> login: " + Arrays.toString(args));
        }
        return method.invoke(this.object, args);
    }

}
