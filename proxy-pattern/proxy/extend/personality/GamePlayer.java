package proxy.extend.personality;

public class GamePlayer implements IGamePlayer {

    private String name = "";

    public GamePlayer(String _name) {
        this.name = _name;
    }

    @Override
    public void login(String userName, String password) {
        System.out.println("login: " + userName + " , " + password);
    }

    @Override
    public void killBoss() {
        System.out.println("kill boss: " + this.name);
    }

    @Override
    public void upgrade() {
        System.out.println(this.name + " upgrade ...");
    }


}
