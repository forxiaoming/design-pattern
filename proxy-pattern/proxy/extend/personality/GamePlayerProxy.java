package proxy.extend.personality;

public class GamePlayerProxy implements IGamePlayer , IProxy {
    private IGamePlayer gamePlayer = null;

    public GamePlayerProxy(IGamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    @Override
    public void login(String userName, String password) {
        System.out.print("proxy ---> ");
        this.gamePlayer.login(userName, password);
    }

    @Override
    public void killBoss() {
        System.out.print("proxy ---> ");
        this.gamePlayer.killBoss();
    }

    @Override
    public void upgrade() {
        System.out.print("proxy ---> ");
        this.gamePlayer.upgrade();
    }

    @Override
    public void count() {
        System.out.println("proxy spend ---> 15.05元");
    }
}
