package proxy.extend.personality;

/**
 * 代理接口
 **/
public interface IProxy {
    /**
     * 计费
     */
    public void count();

}
