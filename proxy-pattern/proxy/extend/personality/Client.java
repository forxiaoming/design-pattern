package proxy.extend.personality;

import java.time.LocalTime;

/**
 * 个性代理
 */
public class Client {

    public static void main(String[] args) {
        IGamePlayer gamePlayer = new GamePlayer("张三");

        GamePlayerProxy proxy = new GamePlayerProxy(gamePlayer);

        System.out.println("Game start time: " + LocalTime.now() + "\n");

        proxy.login("mingming", "123456");
        proxy.killBoss();
        proxy.upgrade();
        proxy.count();

        System.out.println("\n");
        System.out.println("Game stop time: " + LocalTime.now());

    }
}

