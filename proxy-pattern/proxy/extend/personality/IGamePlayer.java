package proxy.extend.personality;

public interface IGamePlayer {

    public void  login(String userName, String password);

    public void killBoss();

    public void upgrade();

}
