package proxy.extend.common;

import java.time.LocalTime;

/**
 * 普通代理
 */
public class Client {

    public static void main(String[] args) {
        GamePlayerProxy proxy = new GamePlayerProxy("ming");

        System.out.println("Game start time: " + LocalTime.now());

        proxy.login("mingming", "123456");
        proxy.killBoss();
        proxy.upgrade();

        System.out.println("Game stop time: " + LocalTime.now());

    }
}

