package proxy.extend.common;

public class GamePlayerProxy implements IGamePlayer {
    private IGamePlayer gamePlayer = null;

    public GamePlayerProxy(String name) {
        try {
            gamePlayer = new GamePlayer(this, name);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void login(String userName, String password) {
        System.out.print("proxy login -----");
        this.gamePlayer.login(userName, password);
    }

    @Override
    public void killBoss() {
        System.out.print("proxy kill boss ----");
        this.gamePlayer.killBoss();
    }

    @Override
    public void upgrade() {
        System.out.print("proxy upgrade ---");
        this.gamePlayer.upgrade();
    }
}
