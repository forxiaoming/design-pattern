package proxy.extend.common;

public class GamePlayer implements IGamePlayer {
    private String name = "";

    /**
     * 通过构造函数加以限制
     */
    public GamePlayer(IGamePlayer _gamePlayer,String _name) throws Exception {
        if (_gamePlayer == null) {
            throw new Exception("不能创建真实角色");
        } else {
            this.name = _name;
        }
    }

    @Override
    public void login(String userName, String password) {
        System.out.println("login: " + userName + " , " + password);
    }

    @Override
    public void killBoss() {
        System.out.println("kill boss: " + this.name);
    }

    @Override
    public void upgrade() {
        System.out.println(this.name + " upgrade ...");
    }
}
