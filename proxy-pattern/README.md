## Proxy Pattern (多代理模式)

### 0. base model

* 原始游戏生涯过程 [./game/process](./game/process)
* 游戏代练公司的出现 [./game/proxy](./game/proxy)

### 1. What ?
Provide a surrogate or placeholder for another object to control access to it.
(为其他对象提供一种代理以控制对这个对象的访问)

#### 1.1. Role ?

* Subject 抽象主题角色: 
* RealSubject 具体主题角色: 业务逻辑的具体执行者.
* Proxy代理主题角色: 限制委托具体主题角色的实现, 并在真实主题角色处理完成前后做预处理和善后工作.

### 2. Use ?

#### 2.1. Advantage ?

* 职责清晰: 真实角色实现业务逻辑.
* 高扩展性: 主题角色不管如何改变, 代理类都无需做任何修改.
* 智能化: 如动态代理.

#### 2.2. Use sence ?
如 Spring AOP(动态代理); 自己不想参与过程的是是非非, 来减轻自身负担;

### 3. Extend

#### 3.1. 普通代理

透明代理与普通代理:

* 透明代理: 代理服务器对用户透明, 用户不知道代理的存在;

* 普通代理: 客户端只能访问代理角色, 不能访问真实角色, 知道代理的存在;
[./proxy/extend/common](./proxy/extend/common)

**Tip: ** 普通代理模式的约束问题尽量通过团队内的编程约规约束.

#### 3.2. 强制代理

* 一般思维是: 通过代理 找到 真实角色,
* 而"强制"是: 通过角色 找到 代理

[./proxy/extend/force](./proxy/extend/force)

#### 3.3. 个性代理

* 代理的目的: 增强 , 在目标的基础之上增强;
* 一个类可以实现不同的接口: 代理类为何不能实现不同接口来完成不同任务?
* 场景: 代理游戏的计费

[./proxy/extend/personality](./proxy/extend/personality)

#### 3.4. 虚拟代理

* 代理类中, 在**需要**的时候才初始化主题对象

#### 3.5. 动态代理 (重头戏)

##### 3.5.1 基本实现

通过实现InvocationHandler接口来产生一个对象的代理对象;

[./proxy/extend/dynamic/demo01](./proxy/extend/dynamic/demo01)

##### 3.5.2 通用动态代理模型

* 抽线主题
* 真实主题
* 动态代理 Handler类
* 动态代理类
* 通知接口及实现类


[./proxy/extend/dynamic/common](./proxy/extend/dynamic/common)

**与静态代理的区别?**

动态代理主要解决"审计"问题, 即横切面的编程, 在不改变已有代码结构的基础上增强或控制对象的行为.

**注意:** 要实现动态代理的首要条件: 被代理类必须实现一个接口. 但是如CGLIB技术可以不需要接口.

### 4. Best Practice

* 可能是大家接触最多的设计模式.
* Spring AOP, AspectJ.
* 调式源码, 看到类似 $Proxy0 这样的结构, 就知道是动态代理了.
* 学习AOP之前的几个概念: Aspect, JoinPoint, Advice, Weave(织入) 